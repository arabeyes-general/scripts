#!/usr/bin/perl
# -*-Perl-*-

#---
# $Id$
#
# ------------
# Description:
# ------------
#  Dictionary upload file generator.
#
#  - Read-in all the .po files
#  - Process and generate an appropriate QaMoose upload file
#  - Process and generate a dictionary "db" file for faster
#    access and process (faster runtimes, less redundancy)
#
# -----------------
# Revision Details:    (Updated by Revision Control System)
# -----------------
#  $Date$
#  $Author$
#  $Revision$
#  $Source$
#
# (www.arabeyes.org - under GPL license)
#---

require "ctime.pl";
require "newgetopt.pl";

use utf8;
use English;
use File::Find;

##
# Keep track of run-time
$global_start_time      = time;
@inputs                 = @ARGV;

##
# Specify global variable values and init some Variables
$this_script            = $0;
$this_script            =~ s|^.*/([^/]*)|$1|;

##
# Find how many spaces the scripts name takes up (nicety)
$this_spaces            = $this_script;
$this_spaces            =~ s/\S/ /g;

##
# Get date and username (record-keeping) along with the path
chop($date = `date`);
chop($pwd  = `pwd`);
$user   = getlogin();

$usage1 = "Usage: $this_script <-dir path> [-output filename]";
$usage2 = "       $this_spaces             [-db_file filename]";
$usage3 = "       $this_spaces             [-no_db]";
$usage4 = "       $this_spaces             [-no_latin]";

# Process the command line
&get_args();

if ( $opt_help ) { &help; }
$proj_dir       = $opt_dir      || &usage(1);
$file_out       = $opt_output   || "qamoose_upload";
$file_dict      = $opt_db_file  || "qamoose_db";

print "<< * >> Using $this_script\n";

# Remove the last slash if there was one (breaks things)
$proj_dir       =~ s|/$||;

# If this is a relative path, absolute it !!
if ( $proj_dir !~ /^\// )
{
    $proj_dir   = "$pwd/$proj_dir";
}

if (!-d $proj_dir )
{
    die "<<!>> ERROR($this_script): $proj_dir doesn't exist \n";
}

# Specify the maximum number of "words" in an output string
$max_string_num = 3;

# What to look for while "grepping"
$orig_string    = 'msgid\s+"';
$tran_string    = 'msgstr\s+"';
$orig_desc      = '^\#\.';

# Global settings
$orig_ext       = "po";

# Keep track of how many new terms we process
$glb_new_run    = 1;
$glb_new_terms  = 0;

# Generate/Process directory with all the files (generate arrays)

%mapping	= (
		   "0x20"	=>	" ",
		   "0x60c"	=>	", ",
		   "0x61b"	=>	"; ",
		   "0x61f"	=>	"?",
		   "0x621"	=>	"'a",
		   "0x622"	=>	"a",
		   "0x623"	=>	"a",
		   "0x624"	=>	"wa'a",
		   "0x625"	=>	"i",
		   "0x626"	=>	"e'",
		   "0x627"	=>	"a",
		   "0x628"	=>	"b",
		   "0x629"	=>	"ah",
		   "0x62a"	=>	"t",
		   "0x62b"	=>	"th",
		   "0x62c"	=>	"j",
		   "0x62d"	=>	"7",
		   "0x62e"	=>	"`7",
		   "0x62f"	=>	"d",
		   "0x630"	=>	"th",
		   "0x631"	=>	"r",
		   "0x632"	=>	"z",
		   "0x633"	=>	"s",
		   "0x634"	=>	"sh",
		   "0x635"	=>	"s",
		   "0x636"	=>	"da",
		   "0x637"	=>	"t",
		   "0x638"	=>	"th",
		   "0x639"	=>	"3",
		   "0x63a"	=>	"`3",
		   "0x640"	=>	"",
		   "0x641"	=>	"f",
		   "0x642"	=>	"qe",
		   "0x643"	=>	"k",
		   "0x644"	=>	"l",
		   "0x645"	=>	"m",
		   "0x646"	=>	"n",
		   "0x647"	=>	"ha",
		   "0x648"	=>	"wo",
		   "0x649"	=>	"a",
		   "0x64a"	=>	"y",
		   "0x64b"	=>	"A",
		   "0x64c"	=>	"D",
		   "0x64d"	=>	"N",
		   "0x64e"	=>	"A",
		   "0x64f"	=>	"D",
		   "0x650"	=>	"I",
		   "0x651"	=>	"",
		   "0x652"	=>	"",
		   "0x660"	=>	"0",
		   "0x661"	=>	"1",
		   "0x662"	=>	"2",
		   "0x663"	=>	"3",
		   "0x664"	=>	"4",
		   "0x665"	=>	"5",
		   "0x666"	=>	"6",
		   "0x667"	=>	"7",
		   "0x668"	=>	"8",
		   "0x669"	=>	"9",
		   "0x66a"	=>	"%",
		   "0x66b"	=>	".",
		   "0x66c"	=>	",",
		   "0x66d"	=>	"*",
		   "0x670"	=>	"",
		  );

if ( !$opt_no_db )
{
    process_dict_db($file_dict);
    open (FILE_DIC, ">> $file_dict") or die "Can't open $file_dict\n";
}

open (FILE_OUT, ">> $file_out") or die "Can't open $file_out\n";

# Do the actual processing
find(\&proc_files, $proj_dir);

close (FILE_OUT);
close (FILE_DIC);

print " < + > Processed $glb_new_terms NEW terms... \n";
print " < + > Generated '$file_out' for QaMoose upload \n";

# Tell 'em how long the whole we were ON !!
&report_run_time($global_start_time);

exit(0);


       ###        ###
######## Procedures ########
       ###        ###

sub process_dict_db
{
    my ($dict_file)     = @_;

    if (-e $dict_file)
    {
        $num_in_db = 0;

        print " < - > Processing dictionary db file - $dict_file  ";
        if ( $opt_debug )
        {
            print "\n";
        }

        open (DB_FILE, "< $dict_file") or die "Can't open $dict_file - $!\n";

        while (<DB_FILE>)
        {
            @line = split(/\:/, $_);
            foreach $entry (@line)
            {
                chomp($entry);
                if ( $entry )
                {
                    $num_in_db++;
                    if ( $opt_debug )
                    {
                        print "Read-out DB - ($entry)\n";
                    }
                    $assoc_dict_db{$entry} = 1;
                }
            }
        }
        if ( !$opt_debug )
        {
            print "($num_in_db)\n";
        }

	close (DB_FILE);
    }
}

##
# Process translated files
sub proc_files
{
    my (
        $filename,
        $sub_out,
        $sub_dir,
        $file_out,
        $num_found,
        $items,
       );

    $found_local = 0;

    if ( ($File::Find::name =~ /\.$orig_ext$/) )
    {
        $full_filename  = $File::Find::name;

        print " < - > Processing - $File::Find::name  ";

        if ( $opt_debug )
        {
            print "\n";
        }

        &proc_single_file( $full_filename );

        if ( !$opt_debug )
        {
            print "($found_local)\n";
        }
    }
}

##
# Process a single translated file (grep, exclude, print)
sub proc_single_file
{
    my($filename)       = @_;

    my(
       $new_file,
       $linenum,
       $str,
       $lo_str,
       $num,
       $file_from_string,
       $print_out_str,
       $tstr,
       $tstr_bad,
      );

    # Note filename in output
    $new_file = 1;

    # Keep track of file line-numbers as we read 'em
    $linenum = 0;

    open (IN_FILE, "< $filename") or die "Can't open $filename - $!\n";

    while (<IN_FILE>)
    {
        $linenum++;

        if (/$orig_desc/)
        {
	    chop;
	    $str_desc = (split(/\"/, $_))[1];
	    next;
	}

        # Processing - Original String
        if (/$orig_string/)
        {
            # Grab the string
            $str        = (split(/\"/, $_))[1];

            # Remove some "no-no" characters - no need for 'em in QaMoose
            $str        = &textize($str);

            # Lowecase everything
            $lo_str     = "\L$str\E";

            # How many words are there ?
            $num        = split(/ /, $str);

            # Skip an entry due to various "limits"
            if (
                !$str                           ||
                ($str =~ /\d+/)                 ||
                ($str =~ /\%/)                  ||
                ($str =~ /\+/)                  ||
                ($str =~ /\|/)                  ||
                ($str =~ /\*/)                  ||
                ($str =~ /\,/)                  ||
                $assoc_dict_term{$lo_str}       ||
                $assoc_dict_db{$lo_str}         ||
                ($num > $max_string_num)
               )
            {
                if ( $opt_debug )
                {
                    if ( $str =~ /\d+/ )
                    {
                        print "Skip w/ num    ($filename, line-$linenum) - '$str'\n";
                    }
                    if ( ($str =~ /\%/) || ($str =~ /\+/) || ($str =~ /\|/) || ($str =~ /\*/) || ($str =~ /\,/) )
                    {
                        print "Skip %+|*,     ($filename, line-$linenum) - '$str'\n";
                    }
                    if ( $assoc_dict_term{$lo_str} )
                    {
                        print "Skip duplicate ($filename, line-$linenum) - '$str'\n";
                    }
                    if ( $assoc_dict_db{$lo_str} )
                    {
                        print "Skip db hit    ($filename, line-$linenum) - '$str'\n";
                    }
                    
                    if ( $num > $max_string_num )
                    {
                        print "Skip max width ($filename, line-$linenum) - '$str'\n";
                    }
                }

                # If something isn't accepted, blow it away
                undef $str;
            }
            else
            {
                if ( $glb_new_run )
                {
                    $glb_new_run         = 0;
                    $file_from_string    = "# Created by $this_script \n";
                    $file_from_string   .= "# - date   : $date \n";
                    $file_from_string   .= "# - pwd    : $pwd \n";
                    $file_from_string   .= "# - user   : $user \n";
                    $file_from_string   .= "# - options: @inputs \n\n";
                }
                else
                {
                    $file_from_string    = "";
                }
                
                # If string is OK, setup what needs to be printed to QaMoose
                $file_from_string .= ( $new_file ? "# From - $filename\n" : "");

                $print_out_str  = "${file_from_string}New_term\n{\n";
                $print_out_str .= "  English      = \($str\)";

                $new_file = 0;
            }

            # Skip all that is below - iterate
            next;
        }

        # Processing - Translated String
        if ($str && /$tran_string/)
        {
            # Grab the string
            $tstr       = (split(/\"/, $_))[1];

            # Remove some "no-no" characters, in case translator mimicked
            $tstr       = &textize($tstr);

	    $tstr_bad   = &bad_utf8($tstr);

	    if ( $tstr_bad )
	    {
		if ( $opt_debug )
		{
		    print "Bad UTF8       ($filename, line-$linenum) - '$str'\n";
		}
	    }
	    else
	    {
		# Make sure we have a valid string
		if ( !$tstr )
		{
		    if ( $opt_debug )
		    {
			print "No Translation ($filename, line-$linenum) - '$str'\n";
		    }
		}
		else
		{
		    # Keep track of local and global term count
		    $found_local++;
		    $glb_new_terms++;

		    # Keep an eye on duplicate entries
		    $assoc_dict_term{$lo_str} = 1;

		    if ( !$opt_no_db )
		    {
			# Really keep an eye on duplicate entries - dump it externally too
			print FILE_DIC ":$lo_str";
			if ( !($glb_new_terms % 10) )
			{
			    print FILE_DIC ":\n";
			}
		    }
                    
		    if ( !$opt_no_latin )
		    {
			$latin = &transpose($tstr);
		    }

		    # Generate rest of upload info
		    print FILE_OUT "$print_out_str \n";
		    print FILE_OUT "  Arabic       = \($tstr\)\n";
		    print FILE_OUT "  Latin        = \($latin\)\n";
		    print FILE_OUT "  Description  = \($str_desc\)\n";
		    print FILE_OUT "}\n";

		    undef $str_desc;
		}
	    }
        }
    }

    close (IN_FILE);
}

##
# Remove some unacceptable characters - these should not be in QaMoose
sub textize
{
    my ($str)   = @_;

    # Remove all that is un-needed
    $str =~ s|\?||g;
    $str =~ s|\!||g;
    $str =~ s|\&||g;
    $str =~ s|\\||g;
    $str =~ s|\/||g;
    $str =~ s|\<||g;
    $str =~ s|\>||g;
    $str =~ s|\(||g;
    $str =~ s|\)||g;
    $str =~ s|\[||g;
    $str =~ s|\]||g;
    $str =~ s|\:||g;
    $str =~ s|\.||g;
    $str =~ s|^\s+||g;
    $str =~ s|\s+$||g;

    return ($str);
}

##
# Check validity of UTF8 string
sub bad_utf8
{
    use utf8;

    my ($utf8_str)      = @_;

    my %ascii_allowed   = (
			   0x09 => 1,            # ASCII - tab
			   0x20 => 1,            # ASCII - space
			   0x21 => 1,            # ASCII - !
			   0x22 => 1,            # ASCII - "
			   0x23 => 1,            # ASCII - #
			   0x24 => 1,            # ASCII - $
			   0x25 => 1,            # ASCII - %
			   0x26 => 1,            # ASCII - &
			   0x27 => 1,            # ASCII - '
			   0x28 => 1,            # ASCII - (
			   0x29 => 1,            # ASCII - )
			   0x2A => 1,            # ASCII - *
			   0x2B => 1,            # ASCII - +
			   0x2C => 1,            # ASCII - ,
			   0x2D => 1,            # ASCII - -
			   0x2E => 1,            # ASCII - .
			   0x2F => 1,            # ASCII - /
			   0x3A => 1,            # ASCII - :
			   0x3B => 1,            # ASCII - ;
			   0x3C => 1,            # ASCII - <
			   0x3D => 1,            # ASCII - =
			   0x3E => 1,            # ASCII - >
			   0x3F => 1,            # ASCII - ?
			   0x40 => 1,            # ASCII - @
			   0x5B => 1,            # ASCII - [ 
			   0x5C => 1,            # ASCII - \ 
			   0x5D => 1,            # ASCII - ]
			   0x5E => 1,            # ASCII - ^
			   0x5F => 1,            # ASCII - _ 
			   0x60 => 1,            # ASCII - ` 
			   0x7B => 1,            # ASCII - {
			   0x7C => 1,            # ASCII - | 
			   0x7D => 1,            # ASCII - } 
			   0x7E => 1,            # ASCII - ~ 
			  );

    # Restrict STDERR a bit
    # - Avoid mysterious "Malformed UTF-8" messages;
    #   after all we're trying to catch 'em
    open (STDERR, ">/dev/null");

    my @chars           = split(//, $utf8_str);

    my $not_utf8        = 0;

    foreach $one_char (@chars)
    {
	my $hex_char     = unpack("U", $one_char);
	if (! ($ascii_allowed{$hex_char} || ($hex_char >= 0x80)) )
	{
	    $not_utf8      = 1;
	}
    }
    # Go back to normal with STDERR :-)
    close (STDERR);
    open STDERR, ">&STDOUT";

    return ($not_utf8);
}

##
#
sub transpose
{
    my ($str_in)	= @_;
    my (
	@chars,
	$elem,
	$str_mid,
	$str_out,
        );

    @chars = split ("", $str_in);

    foreach $elem (@chars)
    {
	$str_mid = unpack("U*", $elem);
	$str_mid = sprintf "%#x", $str_mid;
#	print "($str_mid)";

	if ( defined $mapping{$str_mid} )
	{
	    $str_out .= $mapping{$str_mid};
	}
    }

    return ($str_out);
}

##
# Report the total run-time of the script
sub report_run_time
{
    my ($begin_time)    = @_;
    my (
        $end_time,
        $total_time
       );

    $end_time = time;
    $total_time = $end_time - $begin_time;
  TIME_CASE:
    {
        # Hours
        if (($total_time/3600) > 1)
        {
            printf "<< * >> Run-time = %.2f hours\n",($total_time/3600);
            last TIME_CASE;
        }
        # Minutes
        if (($total_time/60) > 1)
        {
            printf "<< * >> Run-time = %.2f minutes\n",($total_time/60);
            last TIME_CASE;
        }
        # Seconds
        print "<< * >> Run-time = $total_time seconds\n";
    }
}

##
# Display this scripts usage (with or without a death afterwards)
sub usage
{
    my ($die_after)     = @_;

    print "$usage1\n";
    print "$usage2\n";
    print "$usage3\n";
    print "$usage4\n";
    if ($die_after)
    {
        exit(1);
    }
}

##
# Display this scripts usage (with or without a death afterwards)
sub help
{
    &usage(0);
    print <<EOHelp;

-> generate a QaMoose uploadable file from all .po within a tree

  Options:

    <-dir path>         : Specify a path tree - process ALL .po files within
    [-output filename]  : Specify an output filename
    [-db_file filename] : Specify a  dictionary "db" file (to read/write)
    [-no_db]            : Run without using a "db" (do NOT read/write db)
    [-no_latin]         : Run without populating the latin entries

(*) All < > options are mandatory, all [ ] are optional.

EOHelp

    exit(1);
}
##
# Get the command line arguments
sub get_args
{
  &NGetOpt (
            "help",                     # print a nice help screen
            "debug",                    # print various debug output while running
            "dir=s",                    # specify the starting dir of the tree
            "output=s",                 # specify output filename
            "db_file=s",                # specify dictionary "db" file
            "no_db",                    # specify NOT to use the db (don't read, don't write)
            "no_latin",			# specify NOT to fill-in the latin entries
           ) || ( $? = 257, die "Invalid argument\n" );
}
