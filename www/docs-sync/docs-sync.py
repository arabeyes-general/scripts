#! /usr/bin/python
#---
# $Id$
#
# ------------
# Description:
# ------------
#  This script synchronizes SGML documents from a CVS repository and builds the
#  corresponding files (SGML -> HTML, SGML -> PDF etc) for each document.
#  It is supposed to run daily automatically to keep a website up-to-date !
#
# -----------------
# Revision Details:    (Updated by Revision Control System)
# -----------------
#  $Date$
#  $Author$
#  $Revision$
#  $Source$
#---

####################################
#      General Recommendations     #
####################################
#
# doc-sync.cfg is needed (it should contain the list of the documents to create).
# Be sure the AbsPath exists for each document before running the script
# (the RelPath is created by the script however)
# 'jade' and 'htmldoc' must be installed
# Be sure to put the 'ldp.dsl' file in the right directory (under html/ in the dsssl/ directory)
# '~/.cvspass' is needed, filled with correct login information
# For each HTML file created, we run a script (addutf8.awk) to add the charset info, be sure that the script exists.

####################################
#       Possible Enhancements      #
####################################
#
# Test if the output files (HTML, PDF, etc...) are there, even if 'cvs update' says the document is up-to-date
# We should be able to select docs for which we'd like to create html only/pdf only/both/other_formats etc etc
# We should be able to put multiple files in the CVSFiles entry (and only checkout/update the needed files. No need
# to bring all the CVS tree for only a single file).
# Use of the built-in Python TmpFile module
# The addutf8 could be done from the ldp.dsl file's side ?

#Importing Modules
import os
import time
import ConfigParser
import glob

#Variables Definition
CVSUser       = 'arabeyes'
CVSServer     = 'cvs.arabeyes.org'
CVSRepository = '/home/arabeyes/cvs'
CVSMethod     = 'pserver'

LDPFile           = '/usr/share/sgml/docbook/stylesheet/dsssl/modular/html/ldp.dsl'
AddUTF8File       = '/home/arabeyes/bin/www/docs-sync/addutf8.awk'
ConfigurationFile = '/home/arabeyes/etc/docs-sync.cfg'

#For XML
ENCSS    = '/home/arabeyes/work/cvs-working/doc/css/arabeyes.en.css'
ARCSS    = '/home/arabeyes/work/cvs-working/doc/css/arabeyes.ar.css'
SHADE    = '--stringparam shade.verbatim 1'
DEPTH	 = '--stringparam toc.section.depth "4"'
CSS      = '--stringparam html.stylesheet "language.css"'
XSLCHUNK = '/usr/share/sgml/docbook/stylesheet/xsl/ldp/ldp-html-chunk.xsl'
XSLPDF   = '/usr/share/sgml/docbook/stylesheet/xsl/ldp/ldp-print.xsl'

#Functions Definition
def AnyFileChanged(Dir, RefTime):
	for i in os.listdir(Dir):
		if(os.path.getmtime(i) > RefTime):
			return 1

	return 0

#Main Program
print '\ndocs-sync.py/Start *** %s' % (time.strftime('%A, %d %B %Y, %H:%M:%S UTC', time.gmtime()))

InitPath = os.getcwd()

print "Launched from: '%s'" % (InitPath)

if(not os.path.exists(ConfigurationFile)):
        print "File '%s' does not exist !\nExiting !" % ConfigurationFile
        DocumentsNumber = 0

if(not os.path.exists(os.path.expanduser('~/.cvspass'))):
        print "File '~/.cvspass' does not exist !\nExiting !"
	DocumentsNumber = 0

CVSRoot = ':%s:%s@%s:%s' % (CVSMethod, CVSUser, CVSServer, CVSRepository)
Conf = ConfigParser.ConfigParser()
Conf.read(ConfigurationFile)

Documents = Conf.sections()
DocumentsNumber = len(Documents)

print '\nNumber of documents to process: %d\n' % (DocumentsNumber)

for i in range(DocumentsNumber):
	if(i != 0):
		print ''
		
	print 'Processing: %s (%s/%s)' % (Documents[i], i+1, DocumentsNumber)
	
	Lang    = 'EN'
	CSSFile = ENCSS
	
	if(Conf.has_option(Documents[i], 'AbsPath')):
                AbsPath = Conf.get(Documents[i], 'AbsPath')
	else:
		print "Configuration file does not contain 'AbsPath' entry for this document"
                continue

	if(Conf.has_option(Documents[i], 'RelPath')):
                RelPath = Conf.get(Documents[i], 'RelPath')
        else:
                print "Configuration file does not contain 'RelPath' entry for this document"
                continue

	if(Conf.has_option(Documents[i], 'CVSModule')):
                CVSModule = Conf.get(Documents[i], 'CVSModule')
        else:
                print "Configuration file does not contain 'CVSModule' entry for this document"
                continue

	if(Conf.has_option(Documents[i], 'CVSFiles')):
                CVSFiles = Conf.get(Documents[i], 'CVSFiles')
        else:
                print "Configuration file does not contain 'CVSFiles' entry for this document"
                continue

	if(Conf.has_option(Documents[i], 'PDFFile')):
                PDFFile = Conf.get(Documents[i], 'PDFFile')
        else:
                print "Configuration file does not contain 'PDFFile' entry for this document"
                continue

	if(Conf.has_option(Documents[i], 'Format')):
                Format = Conf.get(Documents[i], 'Format')
        else:
                print "Configuration file does not contain 'Format' entry for this document"
                continue
	
	if(Conf.has_option(Documents[i], 'Lang')):
                Lang = Conf.get(Documents[i], 'Lang')
	else:
		print "Configuration file does not contain 'Lang' entry for this document"
		print "We'll assume 'Lang'='EN'..."
	
	if(Format != 'SGML' and Format != 'XML'):
		print "This script does not process %s files yet..." % (Format)
		continue
	
	if(Lang != 'EN' and Lang != 'AR'):
		print "Language %s is not supported !" % (Lang)
		print "We'll assume 'Lang'='EN'..."
		Lang = 'EN'
	
	if(Lang == 'AR'):
		CSSFile = ARCSS
	
	Path = '%s%s' % (AbsPath, RelPath)
	
	if(os.path.exists(Path)):
		if(not os.access(Path, os.W_OK)):
			print 'Not enough permissions to write in: %s' % (Path)
			continue
		
		os.chdir(Path)
		print 'Changed directory to: %s' % (Path)
		
		if(os.path.exists('CVS')):
			InitTime = time.time()
			time.sleep(1)#In order to be able to detect modifications !
			
			try:
				os.system('cvs update -Pd')
			except:
				print 'Failed to update from CVS !'
				continue
			
			if(not AnyFileChanged('.', InitTime)):
				print 'This document is up-to-date !'
				continue
			
		else:
			print "'%s' already exists in a non CVS state !" % (Path)
			continue
	else:
		try:
			os.mkdir(Path)
			print 'Created new directory: %s' % (Path)
			
		except:
			print 'Failed to create new directory: %s' % (Path)
			continue
		
		os.chdir(AbsPath)
		print 'Changed directory to: %s' % (AbsPath)
		os.putenv('CVSROOT', CVSRoot)
		
		try:
			os.system('cvs co -d %s %s' % (RelPath, CVSModule))
		except:
			print 'Failed to checkout from CVS !'
			continue
		
		os.chdir(RelPath)
		print 'Changed directory to: %s' % (RelPath)
		
	try:
		os.system('cp -f %s language.css' % (CSSFile))
				
		print 'Creating HTML output...'
		
		if(Format == 'SGML'):
			os.system('jade -f docs-sync.py.tmp.errfile -E 1000000 -t sgml -d %s#html %s' % (LDPFile, CVSFiles))
			os.system('rm -f docs-sync.py.tmp.errfile')
			for j in glob.glob('*.html'):
				os.system('%s %s' % (AddUTF8File, j))
		
		if(Format == 'XML'):
			os.system('xsltproc %s %s %s %s %s' % (SHADE, DEPTH, CSS, XSLCHUNK, CVSFiles))
			
	except:
		print 'Failed to create HTML files !'
	
	try:
		print 'Creating PDF output...'
		
		if(Format == 'SGML'):
			os.system('jade -f docs-sync.py.tmp.errfile -E 1000000 -t sgml -d %s#html -V nochunks %s > docs-sync.py.tmp.file.html' % (LDPFile, CVSFiles))
			os.system('rm -f docs-sync.py.tmp.errfile')
			os.system('%s docs-sync.py.tmp.file.html' % (AddUTF8File))
			os.system('htmldoc -f %s docs-sync.py.tmp.file.html' % (PDFFile))
			os.system('rm -f docs-sync.py.tmp.file.html')
		if(Format == 'XML'):
			os.system('/opt/fop-0.20.5/fop.sh -xml %s -xsl %s -pdf %s' % (CVSFiles, XSLPDF, PDFFile))
			
	except:
		print 'Failed to create PDF file !'

os.chdir(InitPath)

print "\nBack to: '%s'" % (InitPath)
print 'docs-sync.py/End *** %s' % (time.strftime('%A, %d %B %Y, %H:%M:%S UTC', time.gmtime()))
