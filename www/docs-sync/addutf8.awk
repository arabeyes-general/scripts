#!/usr/bin/awk -f
#---
# $Id$
#
# ------------
# Description:
# ------------
#  This script simply adds the needed charset tags for files created with jade.
#  Thus, browsers would display Arabic right away.
#  Run it on html files freshly created with 'jade'.
#
# -----------------
# Revision Details:    (Updated by Revision Control System)
# -----------------
#  $Date$
#  $Author$
#  $Revision$
#  $Source$
#---

BEGIN {
  Test = 0;
  TMPOutputFile = "_addutf8.awk_tmp_file.txt_";
  
  system("rm -f "TMPOutputFile);
}

$1 {
  if($1 == "><TITLE")
    {
      print "><meta HTTP-EQUIV=\"Content-Type\" CONTENT=\"text/html; charset=utf-8\"><TITLE" >> TMPOutputFile;
      Test = 1;
    }
  else
    print >> TMPOutputFile;
}

END {
  if(Test)
    {
      system("mv -f "TMPOutputFile" "FILENAME);
      print "add charset info in "FILENAME;
    }
  else
    {
      system("rm -f "TMPOutputFile);
      print "did not add any charset info in "FILENAME;
    }
}
