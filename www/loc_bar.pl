#!/usr/bin/perl
# -*-Perl-*-

#---
# $Id$
#
# ------------
# Description:
# ------------
#  HTML localization status bar-graph generator (GENERIC).
#
#  - Idea is to generate a status bar graph for all the files involved.
#  - This script must be made as general/generic as possible (command-line
#    options) so that it functions on any/all localization projects (given
#    appropriate file extnesions and a root-directory).
#
# -----------------
# Revision Details:    (Updated by Revision Control System)
# -----------------
#  $Date$
#  $Author$
#  $Revision$
#  $Source$
#
# (www.arabeyes.org - under GPL license)
#---

require "ctime.pl";
require "newgetopt.pl";

use English;
use File::Find;

##
# Keep track of run-time
$glb_start_time	= time;

##
# Specify global variable values and init some Variables
$this_script    = $0;
$this_script    =~ s|^.*/([^/]*)|$1|;

##
# Find how many spaces the scripts name takes up (nicety)
$this_spaces    = $this_script;
$this_spaces    =~ s/\S/ /g;

# Get some background info
chop($date = `date -u`);
chop($pwd  = `pwd`);
$user	   = getlogin();

# Process the command line
&get_args();

if ( $opt_help ) { &help; }

# User specified options
$proj_dir	= $opt_dir	|| &usage(1);
$proj_name	= $opt_project	|| "kde";
$orig_ext 	= $opt_orig	|| "pot";
$tran_ext 	= $opt_tran	|| "po";
$orig_string	= $opt_ostring	|| 'msgid\s*"(.*)"';
$tran_string	= $opt_tstring	|| 'msgstr\s*"(.*)"';
$tran_string2	= $opt_tstring2	|| '^"\s*[\S\d\w]+\s*[\S\d\w]+';
$plural_string	= 'msgstr\[\d+\]\s*"(.*)"';

# Global web-link prefix
$web_link 	= "http://www.arabeyes.org/misc";

# Unless user wants to use the exact project name input, modify it
if (! $opt_exact )
{
    # Convert the project name to something that is acceptable
    $proj_name	= &munge_project_name($proj_name);
}

# Output file generated
$out_html	= $opt_output	|| "${proj_name}_status_bar.html";

# Attain the actual filename that the user is opting to use
# - need to the info so that the mysql line gets updated with that same name
# - NOTE: there is nothing here to guarantee that the file in mysql is in
#         the right place (its beyond the scope of this script)
$filename_used	= $out_html;
$filename_used	=~ s|^.*/([^/]*)|$1|;

# Note the web-linked fullpath filename to use
$web_link_file	= "$web_link/$filename_used";

# Specify special strings/characters
$glb_comment    = '^#';
$sp_fuzzy	= '^#,\s*fuzzy';

# Remove the last slash if there was one (breaks things)
$proj_dir	=~ s|/$||;

# If this is a relative path, absolute it !!
if ( $proj_dir !~ /^\// )
{
    $proj_dir	= "$pwd/$proj_dir";
}

if (!-d $proj_dir )
{
    die "<<!>> ERROR($this_script): dir '$proj_dir' doesn't exist \n";
}

if (
    ($proj_name ne "KDE")		&&
    ($proj_name ne "Gnome")		&&
    ($proj_name ne "Debian")		&&
    ($proj_name ne "Mandriva")		&&
    ($proj_name ne "Fedora")		&&
    ($proj_name ne "Wordlist")		&&
    ($proj_name ne "OpenOffice")	&&
    ($proj_name ne "Drupal")		&&
    ($proj_name ne "Freedesktop")	&&
    ($proj_name ne "Mozilla")		&&
    ($proj_name ne "Plone")		&&
    ($proj_name ne "ActionApps")
   )
{
    die "<<!>> ERROR($this_script): project '$proj_name' doesn't exist \n";
}

# Generate/Process directory with all the files (generate arrays)
find(\&proc_orig, $proj_dir);
find(\&proc_tran, $proj_dir);

if (! defined %assoc_array_dirs)
{
    die "<<!>> ERROR($this_script): NO '$orig_ext' files in $proj_dir\n";
}

if ( !$opt_debug )
{
    require DBI;

    open (OUTPUT, "> $out_html") or die "Can't open $out_html \n";
    
    &html_print_hdr();
}

# Keep track of overall totals
$tot_orig_all	= 0;
$tot_tran_all	= 0;

foreach $dir (sort (keys %assoc_array_dirs))
{
    # Keep track of directory totals
    $tot_orig_dir = 0;
    $tot_tran_dir = 0;

    foreach $entry (sort (@{$assoc_array_dirs{$dir}}))
    {
	@name_num	= split(/\:/, $entry);
	$name_of_file 	= $name_num[0];
	$orig_value	= $name_num[1];
	$path_file 	= ( $dir ? "$dir/$name_of_file" : $name_of_file );

	# Make things look nice(r) when printed on screen
	$string_length	= length($path_file);
	$spaces		= " " x (45 - $string_length);

	$tot_orig_all	+= $orig_value;
	$tot_orig_dir	+= $orig_value;

	# Grab the translated info upon processing original file (if exists)
	if ( defined $assoc_array_tran{$path_file} )
	{
	    $tran_value   = $assoc_array_tran{$path_file}{value};
	    $mark_value	  = $assoc_array_tran{$path_file}{mark};
	    $percent_done = &proc_nums($orig_value,
				       $assoc_array_tran{$path_file}{value});

	    printf "- $path_file,$spaces %5u, %5u, %3u% $mark_value\n",
		    		$orig_value, $tran_value, $percent_done;
	    &html_print_entry(
			      0,
			      $path_file,
			      $orig_value,
			      $tran_value,
			      $mark_value,
			      $percent_done
			     );

	    $tot_tran_all	+= $tran_value;
	    $tot_tran_dir	+= $tran_value;
	}
	else
	{
	    printf "- $path_file,$spaces %5u,     0,   0%\n", $orig_value;
	    &html_print_entry(
			      0, 
			      $path_file,
			      $orig_value,
			      0,
			      "",
			      0
			     );
	}
    }

    $tot_percent_dir	= &proc_nums($tot_orig_dir, $tot_tran_dir); 
    &html_print_entry(
		      1,
		      "<B><I>$dir - Total</I></B>",
		      $tot_orig_dir,
		      $tot_tran_dir,
		      "",
		      $tot_percent_dir
		     );
}

# Account for the grand total
$tot_percent_all	= &proc_nums($tot_orig_all, $tot_tran_all); 
&html_print_entry(
		  1,
		  "<B>GRAND TOTAL</B>",
		  $tot_orig_all,
		  $tot_tran_all,
		  "",
		  $tot_percent_all
		  );

printf "+ TOTALS ==>\t\t %5u, %5u, %3u%\n", $tot_orig_all,
	                                     $tot_tran_all, $tot_percent_all;
if ( !$opt_debug )
{
    print "--> Generated '$out_html' output HTML file.\n";

    # Update a database entry with new value (for website status)
    print "--> Updating mySQL project '$proj_name' with new percentage\n";
    print "--> Updating mySQL to point at '$web_link_file'\n";
}

if ( !$opt_debug )
{
    &update_db($proj_name, $web_link_file, $tot_percent_all);
    &html_print_legend();
}

if ( !$opt_debug )
{
    close (OUTPUT);
}

# Tell 'em how long the whole we were ON
&report_run_time($glb_start_time);

exit(0);

       ###        ###
######## Procedures ########
       ###        ###

##
# Process the project name string to something acceptable
sub munge_project_name
{
    my ($orig_name)	= @_;

    # Acceptable names,
    # - KDE
    # - Gnome
    # - Wordlist

    my $new_name	= "\L$orig_name\E";

    # Deal with various project names differently
    if ($new_name =~ /^kde/)
    {
	# Uppercase everything
	$new_name	= "\U$orig_name\E";
    }
    else
    {
	# Capitalize first Character only
	$new_name	= "\u$new_name";
    }

    return ($new_name);
}

##
# Process original (non-translated) file
sub proc_orig
{
    my (
	$filename,
	$sub_out,
	$sub_dir,
	$file_out,
	$num_found,
	$items,
       );

    if ( ($File::Find::name =~ /\.$orig_ext/) )
    {
	if ( $_ =~ /(.*)\.($orig_ext[t]*[,v]*$)/ )
	{
	    $filename	= $1;
	}

	$sub_out	= (split(/$proj_dir\//, $File::Find::dir))[1];
	$sub_dir	= ( $sub_out ? "$sub_out/" : "" );

	$file_out	= "$sub_dir$filename";

	($num_found,
	 $ignored)	= &grepify(
				   $File::Find::name,
				   $orig_string,
				   $orig_string,
	    			   $tran_string2
				  );

	$items		= "$filename:$num_found";

	push(@{$assoc_array_dirs{$sub_out}}, $items);
	if ($opt_verbose)
	{
	    print "(+) ORIG in $File::Find::name ($sub_dir, $items)\n";
	}

	# Keep track of files found (ie. remember 'em)
	$assoc_array_found{"$file_out"} = 1;
    }
}

##
# Process translated file
sub proc_tran
{
    my (
	$filename,
	$sub_out,
	$sub_dir,
	$file_out,
	$num_found,
	$items,
       );

    if ( ($File::Find::name =~ /\.$tran_ext*($)/) )
    {
	if ( $_ =~ /(.*)\.($tran_ext[t]*[,v]*$)/ )
	{
	    $filename	= $1;
	}

	$sub_out	= (split(/$proj_dir\//, $File::Find::dir))[1];
	$sub_dir	= ( $sub_out ? "$sub_out/" : "" );

	$file_out	= "$sub_dir$filename";

 	# If this file hasn't been seen yet - its a bare translated file.
        # We'll have to treat it as though it is its own "orig"
	if (! $assoc_array_found{$file_out} )
	{
	    ($num_found,
	     $ignored)	= &grepify(
				   $File::Find::name,
				   $orig_string,
				   $orig_string,
				   $tran_string2
				  );

	    $items	= "$filename:$num_found";

	    push(@{$assoc_array_dirs{$sub_out}}, $items);
	    if ($opt_verbose)
	    {
		print "(*) BARE in $File::Find::name ($file_out)\n";
	    }
	}

	# Now do the normal processing looking for translated strings
	($num_found,
	 $mark_found)	= &grepify(
				   $File::Find::name,
				   $orig_string,
				   $tran_string,
				   $tran_string2,
				   $sp_fuzzy
				  );

	$items 		= "$filename:$num_found";

	$assoc_array_tran{"$file_out"}	= {
	    				   "name"  => $File::Find::name,
					   "value" => $num_found,
					   "mark"  => $mark_found
					  };

	if ($opt_verbose)
	{
	    print "(+) TRAN in $File::Find::name ($sub_dir, $items)\n";
	}
    }
}

##
# Grep for the appropriate strings
sub grepify
{
    my($file_name,
       $p_orig_string,
       $p_tran_string,
       $nxt_tran_string,
       $special_skip)		= @_;

    my $num			= 0;
    my (
	$doing_tran,
	$marker_out,
	$orig_empty,
	$orig_content,
	$orig_check,
	$plural_num,
	$plural_blk,
	$plural_num_found,
       );

    # Indicate if I'm dealing with Orig or Tran strings
    $doing_tran	= defined($special_skip);

    open (FILE, "$file_name") or die "Can't open $file_name \n";

    # Process entire file
    while (<FILE>)
    {
	# See if I have any special characters which I need to skip
	if ( (defined $special_skip) && /$special_skip/)
	{
	    my $within_comment;

	    # Loop on a single block (ie. still next empty line)
	    while (<FILE>)
	    {
		if (/^\s/)
		{ last; }

		# Be wary of commented out special blocks, ignore whole thing
		$within_comment = /$glb_comment/;
	    }
	    # If things were commented out, retain previous value else set it
	    $marker_out = ( $within_comment ? $marker_out : "*" );
	}

	# Skip any and all commented lines
	if (/$glb_comment/)
	{
	    next;
	}

	# Orig validations come into play on translated count only
	if ($doing_tran)
	{
	    if (/^\"Plural-Forms: nplurals=(\d+)/)
	    {
		$plural_num = $1;
	    }

	    # Check on the orig string starting on next line case
	    # - if the orig is deemed empty, ignore the translation
	    $orig_empty	= ( $orig_check && (!(/$nxt_tran_string/)) );
	    $orig_check	= 0;

	    # Make sure my original string has content.
	    # - Exception: strings can be legally empty
	    if (/$p_orig_string/)
	    {
		my $cap_val	= $1;

		# On new gettext pair (ie. msgstr), intialize plural logic
		$plural_blk	  = 0;
		$plural_num_found = 0;

		# On each iteration start anew
		undef($orig_content);

		# - Exception: orig strings can start on next line
		$orig_check	= ($cap_val eq "");

		# Grab pure space strings (ie. empty and non-character)
		if (($cap_val =~ /\s+/) && ($cap_val !~ /\S+/))
		{
		    # Store off value to be used later
		    $orig_content = $cap_val;
		}
	    }
	}

	# Deal with plural form of gettext strings
	if ($doing_tran)
	{
	    # Prime-up our counting to verify number of entries noted
	    if (/^msgid_plural/)
	    {
		$plural_blk = 1;
		next;
	    }

	    # Do the actual count if we are within a plural block
	    if ($plural_blk && ($plural_num > 0) && /$plural_string/)
	    {
		my $cap_val = $1;
		
		# Verify content on eacy plural line
		if ($cap_val =~ /\s*[\S\d\w]+\s*[\S\d\w]*/)
		{
		    $plural_num_found++;
		}
		# Deal with multi-line translations
		else
		{
		    # Get next line
		    $_	= <FILE>;
		    
		    if (/$nxt_tran_string/)
		    {
			$plural_num_found++;
		    }
		}

		# Success is reached if we have a least header number noted
		if ($plural_num_found == $plural_num)
		{
		    $num++;
		}
		next;
	    }
	}

	# Deal with a properly translated strings
	if (/$p_tran_string/)
	{
	    my $cap_val = $1;

	    if (!$orig_empty)
	    {
		# When processing Orig string, catch the valid empties
		if (!$doing_tran &&
		    (($cap_val =~ /\s+/) && ($cap_val !~ /\S+/)))
		{
		    $num++;
		    next;
		}		    

		# If equal empty strings, then its a valid match
		if (defined ($orig_content) && ($orig_content eq $cap_val))
		{
		    if ($opt_verbose)
		    {
			print "(!) Source/Dest spaces - $File::Find::name \n";
		    }
		    $num++;
		    next;
		}

		if ($cap_val =~ /\s*[\S\d\w]+\s*[\S\d\w]*/)
		{
		    $num++;
		    next;
		}
		# Deal with multi-line translations
		else
		{
		    # Get next line
		    $_	= <FILE>;
		    
		    if (/$nxt_tran_string/)
		    {
			$num++;
			next;
		    }
		}
	    }
	}
    }
    close (FILE);
    return ($num, $marker_out);
}

##
# Generate my percentage-done
sub proc_nums
{
    my ($orig_num,
	$tran_num)	= @_;

    if (! $orig_num) 
    { $orig_num = 1; }

    my $num	= (100.0 * $tran_num/$orig_num);

    # Round to nearest integer
    $num	= sprintf("%.0f", $num);

    # A bit of a kludge here (couldn't think of anything better)
    if (($num == 100) && ($tran_num != $orig_num))
    {
	$num = 99;
    }

    return($num); 
}

##
# HTML output file header lines
sub html_print_hdr
{

#<TR bgcolor=lightgrey>
    print OUTPUT qq
|<!-- This file was created by '$this_script'
     - User: $user
     - Date: $date
  -->
<TABLE BORDER=0 CELLSPACING=0 CELLPADDING=2>
<TR bgcolor=bisque>
 <TH align=left>Filename</TH><TH WIDTH=30>&nbsp;</TH>
 <TH>Strings to Translate</TH><TH WIDTH=30>&nbsp;</TH>
 <TH>Strings Translated (done)</TH>
 <TH align=center>%-done</TH>
</TR>
|;

}

##
# HTML output file output - an entry per translated file
sub html_print_entry
{
    # If we're not printing to a file get the heck out of here
    if ( $opt_debug ) { return; }

    my ($do_highlight,
	$file_name,
	$orig_num,
	$tran_num,
	$marker,
	$percent)	= @_;

    my (
	$good,
	$ok,
	$bad,
	$hilite,
	$color,
	$width,
	$highlight
       );

#    $good	= "darkgreen";
#    $ok	= "darkblue";
#    $bad	= "darkred";
#    $hilite	= "lightgrey";
    $good	= "green";
    $ok		= "blue";
    $bad	= "firebrick";
    $hilite	= "gainsboro";

  COLOR_CASE:
    {
        if ( $percent >= 99.5 )
	{ 
	    $color = $good;
	    last COLOR_CASE;
	}
	if ( $percent >= 49.5 )
	{ 
	    $color = $ok;
	    last COLOR_CASE;
	}
	# default
	$color = $bad;
    }
	    
    $width	= ( $percent * 2);
    $highlight	= ( $do_highlight ? " bgcolor=$hilite" : "" );

printf OUTPUT qq
|
<TR$highlight><TD>$file_name</TD><TD>&nbsp;</TD><TD align=center>$orig_num</TD>
 <TD>&nbsp;</TD></TD><TD align=center>$tran_num $marker</TD>
 <TD valign=middle>
  <TABLE><TR><TD bgcolor=$color>
    <IMG SRC="/icons/bar.gif" ALT="bar" HEIGHT=5 WIDTH=$width BORDER=0></TD>
   <TD><FONT SIZE=2><B>%3u%</B></FONT></TD></TR>
  </TABLE></TD></TR>
 </TD></TR>
|, $percent;

}

##
# HTML output file legend entries
sub html_print_legend
{
    print OUTPUT qq
|
</TABLE>
</DL>
<TABLE BORDER=0 CELLPADDING=0 CELLSPACING=0 WIDTH=100%>
  <TR>
    <TH align=right>
    <FONT SIZE=2><I>Last update - ($date)</I></FONT>
    </TH>
  </TR>
</TABLE>
<DL>
  <DT><B>Legend:</B>
    <DD><B>*</B> - File contains "fuzzy" terms/strings.

|;
}

##
# Update mySQL db entry with the new total percentage
sub update_db
{
    my ($pname,
	$web_file,
        $new_percent)	= @_;

    my $db_driver	= "mysql";
    my $db_name		= "arabeyes";
    my $db_table	= "proj_about";
    my $db_username	= "arabeyes";
    my $db_password	= "";

    my $dbh		= &connect_db(
				      $db_driver,
				      $db_name,
				      $db_username,
				      $db_password
				     );

    my $sq_proj		= "${pname}";
    my $sq_status	= qq
|
<a href="$web_file">${new_percent}%</a> done.
|;

    # Quote the value - db requirement
    $sq_proj		= $dbh->quote($sq_proj);
    $sq_status		= $dbh->quote($sq_status);

    # Define command to execute
    my $sql	= qq
|
    UPDATE $db_table
    SET    status 	= $sq_status
    WHERE  proj_name	= $sq_proj
|;
    my $sth	= $dbh->prepare_cached($sql) or
        die "ERROR: Can't execute search query - $DBI::errstr";
    $sth->execute;
    $sth->finish;
    &disconnect_db($dbh);
}

##
# Connect to database
sub connect_db
{
    my ($db_driver,
	$db_name,
	$db_username,
	$db_password)  = @_;

    my $dbh     = DBI->connect(
			       "DBI:$db_driver:$db_name",
			       "$db_username",
                               "$db_password"
			      ) or
		  die "ERROR: Can't connect to the database - $DBI::errstr";

    if (!$ENV{'MOD_PERL'})
    {
        $dbh->{'Warn'}  = 0;
    }

    return($dbh);
}

##
# Disconnect from database
sub disconnect_db
{
    my ($dbh)   = @_;

    $dbh->disconnect or
	die "ERROR: Can't disconnect from database - $DBI::errstr";
}
    
##
# Report the total run-time of the script
sub report_run_time
{
    my ($begin_time)    = @_;

    my (
        $end_time,
        $total_time
       );

    $end_time	= time;
    $total_time = $end_time - $begin_time;

  TIME_CASE:
    {
        # Hours
        if (($total_time/3600) > 1)
        {
            printf "<< * >> Run-time = %.2f hours\n",($total_time/3600);
            last TIME_CASE;
        }
        # Minutes
        if (($total_time/60) > 1)
        {
            printf "<< * >> Run-time = %.2f minutes\n",($total_time/60);
            last TIME_CASE;
        }
        # Seconds
        print "<< * >> Run-time = $total_time seconds\n";
    }
}

##
# Print short usage info
sub usage
{
    my ($die_after) = @_;

    print qq
|Usage: $this_script <-dir root_file_path>  [-project name]
       $this_spaces                        [-orig file_ext]
       $this_spaces                        [-tran file_ext]
       $this_spaces                        [-ostring regexp]
       $this_spaces                        [-tstring regexp]
       $this_spaces                        [-tstring2 regexp]
       $this_spaces                        [-output filename]
       $this_spaces                        [-verbose]
       $this_spaces                        [-debug]
       $this_spaces                        [-help]
|;

    if ( $die_after ) { exit(5); }
}

##
# Print one-liner help
sub help
{
    &usage(0);

    print qq|
-> generates a bar-graph for a specified translation project

  Options:

    <-dir root_file_path>   : Specify root directory of where files reside
    [-project proj_name]    : Specify project name being translated
    [-orig file_ext]        : Specify file extension of original   file (pot)
    [-tran file_ext]        : Specify file extension of translated file (po)
    [-ostring regexp]       : Specify original   regular expression to search
    [-tstring regexp]       : Specify translated regular expresion to search
    [-tstring2 regexp]      : Specify second translated regular expression
    [-output filename]      : Specify output filename to use
    [-verbose]              : Specify to print midway informative statements
    [-debug]                : Specify debug mode (no output file or db update)
    [-help]                 : Specify to print this help menu

|;
    exit(10);
}

##
# Get the command line arguments
sub get_args
{
  &NGetOpt ("dir=s",			# project directory (where files are)
	    "project=s",                # project name
	    "exact",			# use input name as is (no mods)
	    "orig=s",			# original   file extension
            "tran=s",			# transalted file extension
	    "ostring=s",		# original   string to grep for
            "tstring=s",		# translated string to grep for
            "tstring2=s",		# second-line translated string
	    "output=s",			# specify an output file (for html)
	    "verbose",			# print everything out
	    "debug",			# debug mode - don't update file/db
	    "help",                     # print a nice help screen
	   ) || ( $? = 257, die "Invalid argument\n" );
}
