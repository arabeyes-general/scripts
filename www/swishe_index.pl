#!/usr/bin/perl
# -*-Perl-*-

#---
# $Id$
#
# ------------
# Description:
# ------------
#  A script to generate Swish-e search index files so that we
#  can search the contents of Arabeyes.org
#
#  This script is capable of generating multiple indexes via a
#  single invocation.  The current supported lists are noted in
#  the 'run' hash below.
#
# -----------------
# Revision Details:    (Updated by Revision Control System)
# -----------------
#  $Date$
#  $Author$
#  $Revision$
#  $Source$
#
# (www.arabeyes.org - under GPL license)
#---

require "newgetopt.pl";

use English;
use File::Copy;
use File::Spec::Functions qw(:ALL);

##
# Keep track of run-time
$start_time     = time;

##
# Specify global variable values and init some Variables
$this_script    = $0;
$this_script    =~ s|^.*/([^/]*)|$1|;
$version        = " (v - %I%)";

##
# Find how many spaces the scripts name takes up (nicety)
$this_spaces    = $this_script;
$this_spaces    =~ s/\S/ /g;

# Process the command line
&get_args();

if ( $opt_help ) { &help; }

# Find out what user wants to do
if ($opt_web)
{
    push (@do_list, "web");
}

if ($opt_lists)
{
    push (@do_list, "lists");
}

if ($opt_private)
{
    push (@do_list, "priv");
}

# See if there is a reason to proceed
if (!defined(@do_list))
{
    &usage(1);
}

# Let user know that script is running
print "<< * >> Using $this_script$version\n";

# Correct relative paths (if any)
if ( $opt_keep =~ /(~|\.)/ )
{
    chop($pwd  = `pwd`);
    $user      = getlogin();

    # Deal with tilda --> ~/path1/path2
    $opt_keep  =~ s|^~/(.*)?$|"~"."$user/".$1|e;

    # Deal with tilda --> ~username/path1/path2
    $opt_keep  =~ s|^~(\w+)(/.*)?$|(getpwnam($1))[7].$2|e;

    # Deal with dot relatives --> ./path1 or ../path2
    $opt_keep  = rel2abs($opt_keep);
}

# Specify some much needed paths
$swishe_bin		= "/usr/bin/swish-e";
$swishe_conf_dir	= "/home/arabeyes/htdocs/swish-e/conf";
$swishe_log_dir		= $opt_keep || "/home/arabeyes/logs/swish-e";
$copy_to_dir            = $opt_keep || "/home/arabeyes/htdocs/swish-e/data";

%run = (
	web =>
	 {
	     via	=> "-S prog",
	     conf	=> "-c $swishe_conf_dir/run_web.config",
	     out	=> "web.index.swish-e",
	     log	=> "swishe.web.log",
	 },
	lists =>
	 {
	     conf	=> "-c $swishe_conf_dir/run_lists.config",
	     out	=> "lists.index.swish-e",
	     log	=> "swishe.lists.log",
	 },
	priv =>
	 {
	     conf	=> "-c $swishe_conf_dir/run_priv.config",
	     out	=> "priv.index.swish-e",
	     log	=> "swishe.priv.log",
	 },
       );

##
# ----------
# Editorial:
# ----------
# We can do incremental updates using swish-e's "-N" option
# but that doesn't seem worth the headache since our current
# runtimes are reasonably short (swish-e is really fast).
# For further details, 'man swish-e' for the -N option
##

foreach $entry (@do_list)
{
    my $sw_opt	= "$run{$entry}{via} $run{$entry}{conf}";
    my $sw_log	= "$swishe_log_dir/$run{$entry}{log}";
    my $sw_out	= "$run{$entry}{out}";

    print " < * > Building \U$entry\E index\n";
    print " < - > Using '$sw_opt' \n";

    # Doing -- swish-e options > file.log
    system("$swishe_bin $sw_opt > $sw_log 2>&1");

    print "       -> Complete -- logfile = '$sw_log' \n";

    # Should we copy or not ?
    # - Proceed only if we are not instructed to keep things locally
    if ( !$opt_keep || ($opt_keep ne $pwd) )
    {
	if ( !(copy ("$sw_out", "$copy_to_dir/")) )
	{
	    die "<<!>> ERROR($this_script): Can't copy $sw_out : $!\n";
	}
	unlink("$sw_out");
	print " < - > '$sw_out'      copied to $copy_to_dir/ \n";

	if ( !(copy ("$sw_out.prop", "$copy_to_dir/")) )
	{
	    die "<<!>> ERROR($this_script): Can't copy $sw_out.prop : $!\n";
	}
	unlink("$sw_out.prop");
	print " < - > '$sw_out.prop' copied to $copy_to_dir/ \n";
    }
}

# Report out run time to user
&report_run_time($start_time);

exit(0);

       ###        ###
######## Procedures ########
       ###        ###

##
# Report the total run-time of the script
sub report_run_time
{
    my($begin_time) = @_;
    my($end_time);
    my($total_time);
 
    $end_time = time;
    $total_time = $end_time - $begin_time;
    if (($total_time/3600) > 1)     # hours
    {
        printf "<< * >> Total run-time = %.2f",($total_time/3600);
        print " hours\n";
    }
    elsif (($total_time/60) > 1)    # minutes
    {
        printf "<< * >> Total run-time = %.2f",($total_time/60);
        print " minutes\n";
    }
    else                            # seconds
    {
        print "<< * >> Total run-time = $total_time seconds\n";
    }
}

##
# Display this script's usage (with or without a death afterwards)
sub usage
{
    my ($die_after) = @_;

    print "Usage: $this_script <-web> <-lists> <-private> [-keep path]\n";

    if ( $die_after )
    {
	exit(5);
    }
}

##
# Print one-liner help
sub help
{
    &usage(0);

    print qq|
-> regenerate the swish-e search index files

  Options:

    <-web>	        : Indicates to index Arabeyes' webpages
    <-lists>	        : Indicates to index Arabeyes' public  mailing-lists
    <-private>	        : Indicates to index Arabeyes' private mailing-lists
    [-keep path]	: Keep outputs at the indicated dir (don't release)

|;
    exit(10);
}

##
# Get command-line arguments
sub get_args
{
  &NGetOpt (
	    "help",			# Print a help screen
            "web",			# Note web   argument
            "lists",			# Note public  lists argument
            "private",			# Note private lists argument
            "keep=s",			# Don't copy output, keep at dir
	    ) || ( $? = 257, die "Invalid argument\n" );
}
