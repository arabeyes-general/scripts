#!/usr/bin/env python
#
#---
# $Id$
#
# ------------
# Description:
# ------------
# This script will create snapshots out of the projects in the
# CVS repository and make them available for download.
#
# -----------------
# Revision Details:    (Updated by Revision Control System)
# -----------------
#  $Date$
#  $Author$
#  $Revision$
#  $Source$
#
# This program is written under the GNU General Public License.
#---

import os,sys, commands, time, getopt, ConfigParser, MySQLdb

scriptname = os.path.splitext(os.path.basename(sys.argv[0]))[0]
thedate = time.strftime("%Y%m%d")
confile = ''
db_name = ''
db_user = ''
db_pass = ''
cvsroot = ''
cvswork =''
wwwdir = ''
proj_lst = []

def grabargs():
  "Grab all argument values and returns the values in a tuple"
  global confile
  
  if not sys.argv[1:]:
    usage()
    sys.exit(0)

  try:
    opts, args = getopt.getopt(sys.argv[1:], "hc:",
                               ["help", "confile"],)
  except getopt.GetoptError:
    usage()
    sys.exit(0)

  for o, val in opts:
    if o in ("-h", "--help"):
      usage()
      sys.exit(0)
    if o in ("-c", "--confile"):
      confile = val
      readconf()
  
  return None

def readconf():
  "Read configuration file"
  global db_name, db_user, db_pass, cvsroot, cvswork, wwwdir, proj_lst, confile

  config = ConfigParser.ConfigParser()
  try:
    config.readfp(open(confile))
  except IOError:
    print "Unable to open configuration file: %s" % confile
    sys.exit(0)

  if (not config.has_section('Main')):
    print "Bad configuration file. Please refer to documentation. Exiting.."
    sys.exit(1)

  for opt in config.options('Main'):
    cvsroot = config.get('Main', 'CVSROOT')
    cvswork = config.get('Main', 'CVSWORK')
    wwwdir = config.get('Main', 'Download')
    db_name = config.get('Main', 'DB_Name')
    db_user = config.get('Main', 'DB_User')
    db_pass = config.get('Main', 'DB_Pass')
    proj_lst = config.get('Main', 'Projects').split(':')


def tag_date(fname):
  "Tage the date to filename"
  return fname + '-' + thedate + '.tar.bz2'

def cvs_up():
  "Update local copy of CVS"
  os.chdir(cvswork)
  commands.getstatusoutput('cvs update')
  
def dump_oldfiles():
  "Remove older snapshots from directory"
  rm_cmd = 'rm -rf %s*.bz2' % wwwdir
  commands.getstatusoutput(rm_cmd)
  
def create_tarball(proj_name):
  "Create tarballs"

  tar_cmd = 'tar jcvf ' + wwwdir + tag_date(proj_name) + ' ' +  cvswork + proj_name
  print tar_cmd
  commands.getstatusoutput(tar_cmd)
  db_up(proj_name)

def db_up(proj_name):
  "Update Database with latest snapshot"

  connection = MySQLdb.connect(host="localhost", db=db_name,
			       user=db_user, passwd=db_pass)
  con = connection.cursor()
  query1 = 'SELECT proj_id FROM proj_about WHERE proj_name="%s"' \
	   % proj_name
  
  con.execute(query1)
  rs = con.fetchall()
  id = rs[0][0]
  
  query2 = 'UPDATE links_snapshots SET link="download/snapshots/%s", name="%s" WHERE proj_id=%d' \
	   % (tag_date(proj_name), proj_name + '-' + thedate, id)
  con.execute(query2)
  
def usage():
  "Display usage options"
  print "Usage: %s [OPTIONS]" % scriptname
  print "\t[-h | --help             ]\t\toutputs this usage message"
  print "\t[-c | --confile filename ]\t\tuse configuration file"
  print "\r\nThis program is written under the GNU General Public License\n"


def main():
  "Main() function"
  grabargs()
  dump_oldfiles()
  cvs_up()
  map(create_tarball, proj_lst)

if __name__ == "__main__":
  main()
