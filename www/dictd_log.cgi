#!/usr/bin/perl -T
# -*-Perl-*-

#---
# $Id$
#
# ------------
# Description:
# ------------
#  A simplistic means to 'grep' out NEW terms that users were
#  unable to find in the locally running dictd server.  The
#  log files are inspected based on the specified date.
#
# -----------------
# Revision Details:    (Updated by Revision Control System)
# -----------------
#  $Date$
#  $Author$
#  $Revision$
#  $Source$
#
# (www.arabeyes.org - under GPL license - by Nadim Shaikli)
#---

# Let's make perl happy in terms of taint'ness :-)
$ENV{"PATH"}	= undef;

# Load CGI routines/methods
use CGI;	

# Attain the local machines current date
chop($date = `/bin/date`);

($weekday,
 $month,
 $day,
 $time,
 $zone,
 $year) = split(/\s+/, $date);

%month_tran = ( 'Jan' => '01', 'Feb' => '02', 'Mar' => '03',
                'Apr' => '04', 'May' => '05', 'Jun' => '06',
                'Jul' => '07', 'Aug' => '08', 'Sep' => '09',
                'Oct' => '10', 'Nov' => '11', 'Dec' => '12' );

# Create new CGI object
$query = new CGI;

# Dump out the "header" HTML info
print
    $query->header(-charset => "UTF-8"),
    $query->start_html(-title => 'Dictd LOGs', -BGCOLOR => '#E2EBCC'),
    $query->h1('New String dictd log processing');

# Dump out user input form
print
    $query->startform,
    $query->popup_menu(-name	=> 'menu_day',
		       -values	=> [1..31], 
		       -default => $day),
    $query->popup_menu(-name	=> 'menu_month',
		       -values	=> [qw/Jan Feb Mar Apr May Jun
				       Jul Aug Sep Oct Nov Dec/], 
		       -default => $month),
    $query->textfield(-name	=> 'entry_year',
		      -default	=> $year),
    $query->p,
    $query->defaults('Reset to Defaults'),
    $query->submit,
    $query->endform;

# In case there are any errors, handle 'em
$error = $query->cgi_error;

if ($error)
{
    print
	$query->header(-status => $error),
	$query->start_html('Problems'),
	$query->h2('Request not processed'),
	$query->strong($error),
        $query->end_html;
    exit(1);
}

# CGI return values capture
if ( $query->param() )
{
    $select_day   = $query->param('menu_day');
    $select_month = $query->param('menu_month');
    $select_year  = $query->param('entry_year');

    if ( $select_day	&&
	 $select_month	&&
	 $select_year	&&
	 $select_year =~ /\d\d\d\d/ )
    {
	&lookup_file($select_day, $select_month, $select_year);
    }
    else
    {
	print "<STRONG>Error:</STRONG> Proper day/month/year are required<br>";
	print "<STRONG>Note:</STRONG>  Year must be specified with 4 digits";
    }
}

# Dump out the "footer" HTML info
print
    $query->end_html;


       ###        ###
######## Procedures ########
       ###        ###

##
# The 'grep'ing of the dictd log files
sub lookup_file
{
    my ($t_day,
	$t_month,
	$t_year) = @_;

    # Local function variables
    my(
	$in_path,
	$in_file,
	$within,
      );

    $in_path = "/home/arabeyes/logs/dictd";
    $in_file = "$in_path/log." . $t_year . $month_tran{$t_month};
    open (INF, "< $in_file") or die "Can't open $in_file";
    while(<INF>)
    {
	if (/--- RESTARTED \(/)
	{
	    print "Found: $_<br>";
	    $within = 0;
            if (/\D+\s+$t_month\s+$t_day\s+\d+:\d+:\d+\s+$zone\s+$t_year/)
	    {
		$within = 1;
	    }
	}
	if ($within && /\:N\:/)
	{
	    print "$_<br>";
	}
    }
    close(INF);
}
