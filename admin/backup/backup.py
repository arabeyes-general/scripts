#!/usr/bin/env python
#
#---
# $Id$
#
# ------------
# Description:
# ------------
#
# This script performs a backup of a list of directories
# and sends a notification email to the administrator
#
# WARNING: Running this in 'remote' mode can pose a security
# risk, as you are going to create a trust relationship between
# to different machines.
#
# Below is how you can create a trust relationship between the
# host (where you are) and the machine that will have the actual
# backup.
#
# $ ssh-keygen -t dsa
# $ ssh-keygen -t rsa
# $ cd ~/.ssh
# $ cat id_rsa.pub id_dsa.put > authorized_keys2
# $ scp -p authorized_keys2 backup-dest:~/.ssh
#
# -----------------
# Revision Details:    (Updated by Revision Control System)
# -----------------
#  $Date$
#  $Author$
#  $Revision$
#  $Source$
#
#---

import os, sys, time, string, pwd, popen2, syslog, smtplib, commands, getopt, ConfigParser

scriptname = os.path.splitext(os.path.basename(sys.argv[0]))[0]
thedate = time.strftime("%Y%m%d")
dirdate = time.strftime("%b-%d-%Y")
tarcmd = ''
conf = '/etc/backup.conf'
remote = 0
file_lst = []
dir_lst = []
fromaddr = ''
adminaddr = ''
loc = ''
dbname = ''
backupdir = ''
verbose = 0
				    
def grabargs():
    "Grab all command-line aguments"
    global conf, remote, file_lst, dir_lst, adminaddr, loc, dbname, backupdir, verbose
    
    if not sys.argv[1:]:
	readconf()
	sys.exit(0)
    try:
	opts, args = getopt.getopt(sys.argv[1:], "hrf:d:a:l:D:b:c:v",
				   ["help", "remote", "filelist=", "dirlist=", "adminaddr=",
				    "loc=", "dbname=", "backupdir=", "conf=", "verbose"],)
    except getopt.GetoptError:
	usage()
	sys.exit(0)

    for o, val in opts:
	if o in ("-h", "--help"):
	    usage()
	    sys.exit(0)
	if o in ("-c", "--conf"):
	    conf = val
	    readconf()
	if o in ("-r", "--remote"):
	    remote = 1
	if o in ("-f", "--filelist"):
	    file_lst = val
	if o in ("-d", "--dirlist"):
	    dir_lst = val
	if o in ("-a", "--adminaddr"):
	    adminaddr = val
	if o in ("-l", "--loc"):
	    loc = val
	if o in ("-D", "--dbname"):
	    dbname = val
	if o in ("-b", "--backupdir"):
	    backupdir = val
	if o in ("-v", "--verbose"):
	    verbose = 1
	
    return None

def usage():
    "Display usage options"
    print "Usage: %s [OPTIONS]" % scriptname
    print "\t[-h | --help      ]\toutputs this usage message"
    print "\t[-r | --remote    ]\tstream backup tarballs to a remote machine"
    print "\t[-f | --filelist  ]\tlist of file names given to each tarball (see dirlist)"
    print "\t[-d | --dirlist   ]\tlist of directories to be backedup and named by filelist"
    print "\t[-a | --adminaddr ]\temail address of admin to receive notification of backup"
    print "\t[-l | --loc       ]\twhere remote machine is located (e.g. user@remote.com"
    print "\t[-b | --backupdir ]\tDirectory where backups will be stored"
    print "\t[-D | --dbname    ]\tDatabase name to be dumped using 'mysqldump'"
    print "\t[-v | --verbose   ]\tOutput verbosely (for debugging)"
    print "\r\nThis program is written under the GNU General Public License\n"


def readconf():
    "Read configuration file"
    global file_lst, dir_lst, fromaddr, adminaddr, backupdir, dbname, remote
    config = ConfigParser.ConfigParser()
    try:
	config.readfp(open(conf))
    except IOError:
	print "Unable to open configuration file: %s" % conf
	sys.exit(0)

    if (not config.has_section('Main')):
	print "Bad config file. Please refer to documentation. Exiting.."
	sys.exit(1)

    for opt in config.options('Main'):
	file_lst = config.get('Main', 'FileList').split(':')
        dir_lst = config.get('Main', 'DirList').split(':')
	fromaddr = config.get('Main', 'FromAddr')
	adminaddr = config.get('Main','AdminAddr')
	backupdir = config.get('Main', 'BackupDir')
	loc = config.get('Main', 'Destination')
	dbname = config.get('Main', 'DBName')
	remote = config.getboolean('Main', 'Remote')


def check_user():
    "Check to see if user is not root, exit"
    if pwd.getpwuid(os.getuid())[0] != 'root':
        print "You have to be root to run this script!"
        sys.exit(1)
        

def tag_date(fname):
    "Tag the date and extension to file name"
    fname = fname + '-' + thedate + '.tar.bz2'
    return fname

def create_backup_dir():
    "Create backup dir"
    global backupdir
    if (remote):
	mkdircmd = 'ssh ' + loc + ' \"mkdir '
	mkdircmd += backupdir + dirdate + '\"'
    else:
	mkdircmd = 'mkdir ' + backupdir + dirdate

    backupdir = backupdir + dirdate + '/'
    if (verbose):
	print "Making directory: [%s]" % mkdircmd
    commands.getstatusoutput(mkdircmd)

def dobackup(fname, dname):
    "Tar and gzip file and dir"
    global tarcmd

    if (remote):
        tarcmd = 'tar jcvf - ' + dname + ' | ssh ' + loc + ' \"cat > '
        tarcmd += backupdir + dirdate + '/' + fname + '\"'
    else:
        tarcmd = 'tar jcvf ' + backupdir +  fname + ' ' + dname

    if (verbose):
	print "Creating tarball: [%s]" % tarcmd
    commands.getstatusoutput(tarcmd)

def dumpdb(dbname):
    "Dump the SQL Database using 'mysqldump'"
    dumpcmd = 'mysqldump ' + dbname + '> ' + backupdir + dbname + '-' + thedate + '.sql'
    if (verbose):
	print "Dumping database: [%s]" % dumpcmd
    commands.getstatusoutput(dumpcmd)

def notifyadmin():
    "Send notification email to admin"
    try:
        server = smtplib.SMTP('localhost')
        server.sendmail(fromaddr, adminaddr, 'Backup script has run')
        server.quit()
    except:
        syslog.syslog('%s: Unable to send notification email!!' % scriptname)
        

def main():
    "Main() function"
    global file_lst
    
    # check_user()
    grabargs()
    readconf()
    os.chdir('/')
    file_lst = map(tag_date, file_lst)
    if (verbose):
	print 'file_lst: %s' % file_lst
	print 'dir_lst: %s' % dir_lst
	print 'backupdir: %s' % backupdir
    create_backup_dir()
    map(dobackup, file_lst, dir_lst)
    if (dbname):
	dumpdb(dbname)
    syslog.syslog('%s: Running the daily backup' % scriptname)
    notifyadmin()
    
    sys.exit(0)


if __name__ == "__main__":
    main()
