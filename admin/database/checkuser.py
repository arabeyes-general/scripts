#! /usr/bin/python
#---
# $Id$
#
# ------------
# Description:
# ------------
#  This script checks a MySQL user table and sends notifications for
#  users with some inactivity time. The notification is renewed with a regular step.
#  If the users don't react (by cvs commit or login or email), they stop receiving notifications
#  and their accounts should be deleted 'manually'.
#
# -----------------
# Revision Details:    (Updated by Revision Control System)
# -----------------
#  $Date$
#  $Author$
#  $Revision$
#  $Source$
#---

####################################
#       Possible Enhancements      #
####################################
#
#Implement Cc and Bcc in the SendEmail Function
#Send notification when the account is deleted
#Send a ThankYou when status becomes 'active' again ;-)

#Importing Modules

import sys
import time
import smtplib
from email.MIMEText import MIMEText

sys.path.append("/home/arabeyes/bin/lib/")
from aelib import *

#Variables Definition

EmailHostName  = 'localhost'
EmailSubject   = 'Account Warning'
FromAddress    = 'Arabeyes Accounts\' Manager <%s>' % (SupportEmail)
ReplyToAddress = 'Arabeyes Support <%s>' % (SupportEmail)

InactivityLimit = 60 #This Value is in days
WarnLimit       = 30 #This Value is in days
WarnStep        = 14 #This Value is in days
#WarnLimit should not be a multiple of WarnStep. Ideally, it should be: WarnLimit = n * WarnStep + 1, n being a postive integer > 0.

#Functions Definition

def SendEmail(HostName, From, ReplyTo, To, Cc, Bcc, Subject, Content):#the function does not support Cc and Bcc yet
	try:
		Message = MIMEText(Content)
		
		Message['MIME-Version'] =  '1.0'
		Message['Content-type'] = 'text; charset=utf-8'
		Message['From'] = From
		Message['Date'] = time.strftime('%a, %d %b %Y %H:%M:%S UTC', time.gmtime())
		Message['Reply-To'] = ReplyTo
		Message['To'] = To
		#	Message['Cc'] = Cc
		#	Message['Bcc'] = Bcc
		Message['Subject'] = Subject
		Message['X-Priority'] = '1'
		Message['X-MSMail-Priority'] = 'High'
		Message['X-Mailer'] = 'Arabeyes.org'	
		
		Server = smtplib.SMTP(HostName)
		Server.sendmail(From, [To], Message.as_string())
		#	Server.sendmail(From, [Cc], Message.as_string())
		#	Server.sendmail(From, [Bcc], Message.as_string())
		Server.quit()
	except:
		print 'Problem while trying to send an email to %s !' % (To)

def BuildMessage(FirstName, InactivityLimit, DeleteTime, ContactEmail, UserName, NiceCreated, NiceLastLogin, NiceLastCommit):
	Message = 'Salam %s,\n\nThis automated email is sent to you (a registered Arabeyes.org member) due to the lack of activity on your account (we maintain and host active participants only).  In short, you have not logged into Arabeyes.org\'s website and/or used its CVS commit privileges for at least %s days.\n\nIf your current status doesn\'t change within %s days from the date this email was sent, your account (ability to login to Arabeyes and/or be considered a CVS writer) will unfortunately be deactivated and deleted. In order to remedy the situation you can do one of the following,\n\n1. Login to the Arabeyes.org website. If you forgot your password, select the \'Forgot Password\' option on our website in order to have a regenerated new password be emailed to you.\n\n2. Post to the various mailing-lists seeking assistance on what to work on next in order to reignite your participation which will ultimately lead you to an opportunity to CVS commit a useful/needed change/file.\n\nFor any further requests or in case of a problem (you don\'t have a username yet for example), please contact us as soon as possible at,\n\n%s\n\nSome useful information,\n---\nYour username      : %s\nAccount created on : %s\nLast login         : %s\nLast CVS commit    : %s\n---\n\nHoping to hear from you very soon !\n\nPS: If you realize this as an error, please let us know as soon as possible !\n\nIf you\'ve decided not to keep your account active with Arabeyes, simply ignore this message; your account will get deactivated automatically in due course.\n\n.: Arabeyes.org Janitorial Services ;-) :.\n' % (FirstName, InactivityLimit, DeleteTime, ContactEmail, UserName, NiceCreated, NiceLastLogin, NiceLastCommit)
	
	return Message

#Main Program

print '\ncheckuser.py/Start *** %s' % (time.strftime('%A, %d %B %Y, %H:%M:%S UTC', time.gmtime()))

DBCursor.execute("select id,fname,lname,active,UNIX_TIMESTAMP(warndate),UNIX_TIMESTAMP(lastlogin),UNIX_TIMESTAMP(created),email,username from user order by id")

QueryResult = DBCursor.fetchall()

ActualTime = time.time()
WarnDate = time.strftime('%Y%m%d%H%M%S', time.gmtime())

TotalNbr    = 0
WarnedNbr   = 0
ActiveNbr   = 0
InactiveNbr = 0
DeletedNbr  = 0

ActiveGuys   = '%-10s %-15s %-15s (%-15s) %-15s %-15s %-15s %-15s\n---\n' % ('UserId', 'FirstName', 'LastName', 'UserName', 'Created', 'LastCommit', 'LastLogin', 'LastWarning')
InactiveGuys = '%-10s %-15s %-15s (%-15s) %-15s %-15s %-15s %-15s\n---\n' % ('UserId', 'FirstName', 'LastName', 'UserName', 'Created', 'LastCommit', 'LastLogin', 'LastWarning')
WarnedGuys   = '%-10s %-15s %-15s (%-15s) %-15s %-15s %-15s %-15s\n---\n' % ('UserId', 'FirstName', 'LastName', 'UserName', 'Created', 'LastCommit', 'LastLogin', 'LastWarning')
DeletedGuys  = '%-10s %-15s %-15s (%-15s) %-15s %-15s %-15s %-15s\n---\n' % ('UserId', 'FirstName', 'LastName', 'UserName', 'Created', 'LastCommit', 'LastLogin', 'LastWarning')

for QueryRow in QueryResult:
	TotalNbr = TotalNbr+1
	UserLastCommit = GetUserLastCommit(QueryRow[0])
	
	InactivityTime = int((ActualTime-UserLastCommit)/86400.)
	LastLoginTime  = int((ActualTime-QueryRow[5])/86400.)
	WarnTime       = int((ActualTime-QueryRow[4])/86400.)
	CreationTime   = int((ActualTime-QueryRow[6])/86400.)
	
	NiceCreated     = time.strftime('%Y%m%d', time.gmtime(QueryRow[6]))
	NiceLastCommit  = time.strftime('%Y%m%d', time.gmtime(UserLastCommit))
	NiceLastLogin   = time.strftime('%Y%m%d', time.gmtime(QueryRow[5]))
	NiceLastWarning = time.strftime('%Y%m%d', time.gmtime(QueryRow[4]))
	
	if(InactivityTime >= InactivityLimit and LastLoginTime >= InactivityLimit and CreationTime >= InactivityLimit):
		if(QueryRow[3] == 'Y'):
			InactiveNbr = InactiveNbr+1
			InactiveGuys = '%s%-10d %-15s %-15s (%-15s) %-15s %-15s %-15s %-15s\n' % (InactiveGuys, QueryRow[0], QueryRow[1], QueryRow[2], QueryRow[8], NiceCreated, NiceLastCommit, NiceLastLogin, NiceLastWarning)
			DBCursor.execute("update user set active='N',warndate='%s' where id=%s" % (WarnDate, QueryRow[0]))
			SendEmail(EmailHostName, FromAddress, ReplyToAddress, '%s %s <%s>' % (QueryRow[1], QueryRow[2], QueryRow[7]), '', '', EmailSubject, BuildMessage(QueryRow[1], InactivityLimit, WarnLimit-WarnTime, SupportEmail, QueryRow[8], NiceCreated, NiceLastLogin, NiceLastCommit))
		else:
			if(WarnTime%WarnStep == 0 and WarnTime < WarnLimit and WarnTime > 0):
				WarnedNbr = WarnedNbr+1
				WarnedGuys = '%s%-10d %-15s %-15s (%-15s) %-15s %-15s %-15s %-15s\n' % (WarnedGuys, QueryRow[0], QueryRow[1], QueryRow[2], QueryRow[8], NiceCreated, NiceLastCommit, NiceLastLogin, NiceLastWarning)
				SendEmail(EmailHostName, FromAddress, ReplyToAddress, '%s %s <%s>' % (QueryRow[1], QueryRow[2], QueryRow[7]), '', '', EmailSubject, BuildMessage(QueryRow[1], InactivityLimit, WarnLimit-WarnTime, SupportEmail, QueryRow[8], NiceCreated, NiceLastLogin, NiceLastCommit))
			elif(WarnTime >= WarnLimit):
				DeletedNbr = DeletedNbr+1
				DeletedGuys = '%s%-10d %-15s %-15s (%-15s) %-15s %-15s %-15s %-15s\n' % (DeletedGuys, QueryRow[0], QueryRow[1], QueryRow[2], QueryRow[8], NiceCreated, NiceLastCommit, NiceLastLogin, NiceLastWarning)
			
	else:
		if(QueryRow[3] == 'N' and InactivityTime < InactivityLimit):
			ActiveNbr = ActiveNbr+1
			ActiveGuys = '%s%-10d %-15s %-15s (%-15s) %-15s %-15s %-15s %-15s\n' % (ActiveGuys, QueryRow[0], QueryRow[1], QueryRow[2], QueryRow[8], NiceCreated, NiceLastCommit, NiceLastLogin, NiceLastWarning)
			DBCursor.execute("update user set active='Y' where id=%s" % (QueryRow[0]))

print '\n%-10d users\' accounts have been processed,' % (TotalNbr)

if(ActiveNbr > 0):
	print '\n%-10d users became active.' % (ActiveNbr)
	print ActiveGuys

if(InactiveNbr > 0):
	print '\n%-10d users became inactive (warning email sent).' % (InactiveNbr)
	print InactiveGuys

if(WarnedNbr > 0):
	print '\n%-10d users have been warned once more.' % (WarnedNbr)
	print WarnedGuys

if(DeletedNbr):
	#(the web interface must be working, which is not the case yet)
        #print '(users who did not reply to their warning email)\nPlease use the WEB INTERFACE to delete accounts and NOTHING ELSE!!!\n'
	print '\n%-10d users\' accounts must be deleted.' % (DeletedNbr)
	print DeletedGuys

print '\ncheckuser.py/End *** %s' % (time.strftime('%A, %d %B %Y, %H:%M:%S UTC', time.gmtime()))
