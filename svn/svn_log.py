#!/usr/bin/env python
#
#---
# $Id$
#
# ------------
# Description:
# ------------
#  This script sends notification mail to the CVS mailing-list
#  upon each commit to the repository in addition to inserting
#  the commit log info in a mysql database.
#  It is called by CVSROOT/loginfo
#
# -----------------
# Revision Details:    (Updated by Revision Control System)
# -----------------
#  $Date$
#  $Author$
#  $Revision$
#  $Source$
#
#---

#Importing Modules

import os, sys, time, string, smtplib, pwd, syslog
import getopt

sys.path.append("/home/arabeyes/bin/lib/")
from aelib import *

#Variables Definition

scriptname  = sys.argv[0].split('/')[-1]
commit_time = time.strftime("%Y%m%d%H%M%S", time.gmtime())
committer   = os.getenv('CVS_USER') or pwd.getpwuid(os.getuid())[0]
hostname    = 'localhost'

#
# Functions
#

def usage():
  "Script usage output"
  print "Usage: %s  [-n|--hostname= hostname]" \
	% scriptname
  print "\t\t\t[-v|--revision= revision]"
  print "\t\t\t[-p|--project= project]"
  print "\t\t\t[-h|--help]"


def grabargs():
  "Grab all the argument values"
  global hostname

  if not sys.argv[1:]:
    usage()
    sys.exit(0)

  try:
    opts,args = getopt.getopt(sys.argv[1:], "h:n:v:p:", ["help",
                    "hostname=", "revision=", "project="])
  except getopt.GetoptError:
    usage()
    sys.exit(2)
  for o,val in opts:
    if o in ("-h", "--help"):
      usage()
      sys.exit(0)
    if o in ("-n", "--hostname"):
      hostname = val
    if o in ("-v", "--revision"):
      revision = val
    if o in ("-p", "--project"):
      projname = val

def db_setMsg(UserId, msg):
  "Store commit log into db"

  #beg = msg.find('Update of')
  #end = msg.find('\n', beg)

  #projname = CvsLog2ProjectName(msg)
  projid   = ProjectName2Id(projname)
  #projname, projid = projlist[msg[beg+29:end]].split(':')

  try:
    msg = DB.escape_string(msg)
  
    DBCursor.execute('INSERT INTO cvs set userid=%d, logdate="%s", logmsg="%s", projectid=%d' % (UserId, commit_time, msg, projid))
  except:
    syslog.syslog("%s: [db_setMsg]: Exception trying to insert log to db" % (scriptname))

#
# main() 
#

def main():
  grabargs()
  global subject

  thedate = time.asctime(time.gmtime())

  #UserId needs to be global
  UserId   = UserName2Id(committer)
  fullname = UserId2RealName(UserId)

  db_setMsg(UserId, msg[1])

if __name__== "__main__":
  main()
