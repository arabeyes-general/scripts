#!/usr/bin/awk -f
#---
# $Id$
#
# ------------
# Description:
# ------------
#  This script adds ':cvsuser' for an entry in a passwd file
#  Run 'addcvsuser.awk u=someuser passwd' right after 'htpasswd -b passwd someuser somepass'
#
# -----------------
# Revision Details:    (Updated by Revision Control System)
# -----------------
#  $Date$
#  $Author$
#  $Revision$
#  $Source$
#---

BEGIN {
  Test = 0;
  TMPOutputFile = "_addcvsuser.awk_tmp_file.txt_";
  
  system("rm -f "TMPOutputFile);
}

$1 {
  if(!u)
    {
      print "usage: addcvsuser.awk u='username'";
      exit;
    }

  split($0, vec,":");
  
  if(vec[1] == u && vec[3] != "cvsuser")
    {
      print $0":cvsuser" >> TMPOutputFile;
      Test = 1;
    }
  else
    print >> TMPOutputFile;
}

END {
  if(Test)
    {
      system("mv -f "TMPOutputFile" "FILENAME);
      print "updated entry for user '"u"' in "FILENAME;
    }
  else
    {
      system("rm -f "TMPOutputFile);
      print "did not update any entry in "FILENAME;
    }
}
