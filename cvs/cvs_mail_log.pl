#!/usr/bin/perl
# -*-Perl-*-

#---
# $Id$
#
# ------------
# Description:
# ------------
#  Script to send mail upon CVS commit (to reside in
#  CVSROOT's loginfo).
#
# -----------------
# Revision Details:    (Updated by Revision Control System)
# -----------------
#  $Date$
#  $Author$
#  $Revision$
#  $Source$
#
#---

require "newgetopt.pl";

use User::pwent;

# Retain script name
$this_script	= $0;
$this_script	=~ s|^.*/([^/]*)|$1|;

&get_args();

chop($date = `date`);

# Get runner's login and fullname
$logname	= getlogin() || $opt_name;
#$pw		= getpwnam($logname);
#$fullname	= (split(/,/, $pw->gecos))[0];

# Who is this mail sent to
$to		= 'cvs';

# Remove those freaky single quotes
$opt_title	=~ s/[\'\"]+[\s+]*//g;

# Mail command to use
$mail		= "/usr/sbin/sendmail -t -oi";

open (PIPE_OUT, "| $mail") or die "Can't open pipe - '$mail'\n";

# Mail header info
#print PIPE_OUT "From: $fullname <$logname\@arabeyes.org>\n";
print PIPE_OUT "To: $to\n";
print PIPE_OUT "Subject: $opt_title\n\n";

# Mail body info (actual message)
print PIPE_OUT "------------------------------------------------------\n";
print PIPE_OUT "From: $logname\n";
print PIPE_OUT "Date: $date\n";
print PIPE_OUT "\n";

# The actual pipe'ed-in log message
while (<>)
{
    # Add an empty line before "Log Message" - looks nicer :-)
    if (/Log Mess/)
    {
	print PIPE_OUT "\n";
    }
    print PIPE_OUT $_;
}

close(PIPE_OUT);
exit(0);

       ###        ###
######## Procedures ########
       ###        ###

##
# Get command-line arguments
sub get_args
{
    &NGetOpt (
	      "name=s",			# name of commitor/user
	      "title=s",		# title string for email to send
	      ) || ( $? = 257, &error("Invalid argument") );
}
