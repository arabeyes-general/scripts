#!/usr/bin/env python
#
#---
# $Id$
#
# ------------
# Description:
# ------------
#  This script sends notification mail to the CVS mailing-list
#  upon each commit to the repository in addition to inserting
#  the commit log info in a mysql database.
#  It is called by CVSROOT/loginfo
#
# -----------------
# Revision Details:    (Updated by Revision Control System)
# -----------------
#  $Date$
#  $Author$
#  $Revision$
#  $Source$
#
#---

#Importing Modules

import os, sys, time, string, smtplib, pwd, syslog
import getopt

sys.path.append("/home/arabeyes/bin/lib/")
from aelib import *

#Variables Definition

scriptname  = sys.argv[0].split('/')[-1]
commit_time = time.strftime("%Y%m%d%H%M%S", time.gmtime())
committer   = os.getenv('CVS_USER') or pwd.getpwuid(os.getuid())[0]
hostname    = 'localhost'
subject     = ''
toaddr      = ''

#
# Functions
#

def usage():
  "Script usage output"
  print "Usage: %s  [-n|--hostname= hostname] [-t|--subject= 'subjectname']" \
	% scriptname
  print "\t\t\t[-l|--listname= mailing-list-name] [-v|--revision= revision]"
  print "\t\t\t[-h|--help]"


def grabargs():
  "Grab all the argument values"
  global hostname, subject, toaddr

  if not sys.argv[1:]:
    usage()
    sys.exit(0)

  try:
    opts,args = getopt.getopt(sys.argv[1:], "ht:n:l:c:v:", ["help", "subject=",
                    "hostname=", "listname=", "revision="])
  except getopt.GetoptError:
    usage()
    sys.exit(2)
  for o,val in opts:
    if o in ("-h", "--help"):
      usage()
      sys.exit(0)
    if o in ("-t", "--subject"):
      subject = val
    if o in ("-n", "--hostname"):
      hostname = val
    if o in ("-l", "--listname"):
      toaddr = val
    if o in ("-v", "--revision"):
      revision = val

def init_logmsg(fullname, fromaddr):

  thedate = time.asctime(time.gmtime())
  dblogmsg = ''
  
  msg = ("From: %s\r\nSubject: %s\r\nTo: %s\r\n\r\n"
         % (fromaddr, subject, toaddr))
  logmsg =  "\r\n----------------------------------------------\r\n"
  logmsg += "From: %s (%s)\r\n" % (fullname, committer) 
  logmsg += "Date: %s\r\n\r\n" % thedate
  logmsg += "Changeset: http://cvs.arabeyes.org/viewcvs/%s?%s\r\n" % (path, revision)

  while 1:
    try:
      line = sys.stdin.readline()
      if line.find('Log Message') >= 0:
        line = '\n' + line
    except EOFError:
      break
    if not line:
      break

    logmsg = logmsg + line
    dblogmsg = dblogmsg + line

  return (msg + logmsg, dblogmsg)

def sendnotify(hostname, fromaddr, toaddr, msg):
  "Send notification email to CVS mailing-list"
  try:
    server = smtplib.SMTP(hostname)
    #server.set_debuglevel(1)
    server.sendmail(fromaddr,toaddr,msg)
    server.quit()
  except:
    syslog.syslog('%s: Unable to send message from %s to CVS list'
                   % (scriptname, fromaddr))
  else:
    syslog.syslog('%s: Succesfully sent message from %s to CVS list'
                   % (scriptname, fromaddr))

def db_setMsg(UserId, msg):
  "Store commit log into db"

  #beg = msg.find('Update of')
  #end = msg.find('\n', beg)

  projname = CvsLog2ProjectName(msg)
  projid   = ProjectName2Id(projname)
  #projname, projid = projlist[msg[beg+29:end]].split(':')

  try:
    msg = DB.escape_string(msg)
  
    DBCursor.execute('INSERT INTO cvs set userid=%d, logdate="%s", logmsg="%s", projectid=%d' % (UserId, commit_time, msg, projid))
  except:
    syslog.syslog("%s: [db_setMsg]: Exception trying to insert log to db" % (scriptname))

#
# main() 
#

def main():
  grabargs()
  global subject

  thedate = time.asctime(time.gmtime())

  #UserId needs to be global
  UserId   = UserName2Id(committer)
  fullname = UserId2RealName(UserId)

  fromaddr = '\"' + fullname + '\" <' + committer + '@' + hostname + '>'
  subject = subject.replace("\"","")

  msg = init_logmsg(fullname, fromaddr)

  ignorestr = string.upper(msg[0])
  if ignorestr.find('CVS_SILENT') >=0:
    sendnotify('localhost', fromaddr, committer, msg[0])#isn't supposed to be no notify but setDB in this case !?
  else:
    sendnotify('localhost', fromaddr, toaddr, msg[0])
    db_setMsg(UserId, msg[1])

if __name__== "__main__":
  main()
