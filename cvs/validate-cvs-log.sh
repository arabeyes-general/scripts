#!/bin/sh
#
# Check to see if log message is too short to
# accept (< 10 characters it too short)
#
# This script is called by CVSROOT/verifymsg
#
# $Id$

if [ `cat $1 | wc -c ` -lt 10 ] ; then
echo "CVS Log message too short. Please enter a description for the changes made."
  exit 1
else
  exit 0
fi
