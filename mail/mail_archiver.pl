#!/usr/bin/perl
# -*-Perl-*-

#---
# $Id$
#
# ------------
# Description:
# ------------
#  Archive incoming mail-list messages in a Year/Month format.
#  Archive stored   mail-list messages in a Year/Month format.
#
# -----------------
# Revision Details:    (Updated by Revision Control System)
# -----------------
#  $Date$
#  $Author$
#  $Revision$
#  $Source$
#
#---

$this_script    = $0;
$this_script    =~ s|^.*/([^/]*)|$1|;

# Set appropriate umask
umask(002);

if ( $ARGV[0] =~ /\-/ )
{
    # Below kinda sucks, I'm opting to go this way instead of using
    # "get_args" globally since I'm trying to make this script as
    # light-weight and FAST in "normal-mode" as possible.
    #
    # We should only fall into this 'if' section when we're recreating
    # directories by hand (ie. NOT "normal-mode :-).
    require "newgetopt.pl";

    &get_args();

    $filename	= $opt_filename	|| &error("Need -filename");
    if (! -e $filename ) { &error("$filename doesn't exist") };
    $listname	= $opt_listname	|| &error("Need -listname");
#    $hostname   = $opt_host	|| &error("Need -host");
    $month	= $opt_month	|| &error("Need -month");
    $year	= $opt_year	|| &error("Need -year");
    &proc_y_m;
}
else
{
    chop($year	= `date +%Y`);
    chop($month	= `date +%B`);
#   $listname	= $ARGV[0] or &error("No listname supplied.");
    $listname	= $ARGV[0] or &usage();
#    $hostname   = $ARGV[1];
}

# UGLY HACK BELOW - we should get hostname passed-in
if ($listname =~ /^esc_/)
{
    $hostname = "escarab";
}
else
{
    $hostname = "arabeyes";
}
print "List = $listname | opting for $hostname \n";

$archive_dir	= "/home/$hostname/htdocs/lists/archives";
$listname_dir	= "$archive_dir/$listname";
$y_m		= "$year/$month";
$msg_dir	= "$listname_dir/$y_m";
$index		= "index.html";

# Specify MHonArc's configuration file (multi-index purpose)
$mh_rcfile	= "/etc/mhonarc/top.mrc";

# Check on existance of message directory
if ( opendir(MSG_DIR, $msg_dir) )
{
    # If directory is there; files are there too - just run archiver :-)
    closedir(MSG_DIR);
}
else
{
    # Directory doesn't exist - create new year/month & index file
    system "mkdir -p $msg_dir" || &error("Can't open $msg_dir");

    &process_index();
}

$mhonarc	= "/usr/bin/mhonarc \\
  		     -add \\
		     -rcfile $mh_rcfile \\
  		     -outdir $msg_dir";

system("$mhonarc $filename");

exit(0);

       ###        ###
######## Procedures ########
       ###        ###

##
# Process the index file in the $listname directory
sub process_index
{
    # Grab the index file
    opendir(LISTNAME_DIR, $listname_dir) or &error("Can't open $listname_dir");

    @index_file = grep(/$index/i, readdir (LISTNAME_DIR));
    closedir(LISTNAME_DIR);

    if (defined @index_file)
    {
	# index file is there - let's add our entry and bolt
	&add_index_entry;
    }
    else
    {
	# OK this is a new list - let's create index.html
	&create_index;

	# index file is there now :-) let's add our entry
	&add_index_entry;
    }
}    

##
# Add an entry to the index file above all the other entries already there.
# The idea here to have the most recent month on top.
sub add_index_entry
{
    open (IN_FILE, "<$listname_dir/$index") or
	&error("Can't open $listname_dir/$index");

    while (<IN_FILE>)
    {
	if (/<table/i)
	{
 	    push(@output, $_);
 	    push(@output, "<tr><td align=right><B>$month, $year:\n");
 	    push(@output, "</td><td>[<A HREF=\"./$y_m/index.html\">Thread Index</A>]\n");
 	    push(@output, "     <td>[<A HREF=\"./$y_m/date.html\">Date Index</A>]\n");
	    push(@output, "     <td>[<A HREF=\"./$y_m/author.html\">Author Index</A>]\n");
	    push(@output, "     <td>[<A HREF=\"./$y_m/subject.html\">Subject Index</A>]\n");
	}
	else
	{
	    push(@output, $_);
	}
    }
    close(IN_FILE);

    open (OUT_FILE, ">$listname_dir/$index") or
	&error("Can't create $listname_dir/$index");

    foreach $line (@output)
    {
	print OUT_FILE $line;
    }
    close(OUT_FILE);
}

##
# Create an html index file -- I guess its either a new year or/and new list
sub create_index
{
    open (OUTPUT, "> $listname_dir/$index") or
	&error("Can't create $listname_dir/$index");

    print OUTPUT <<EOindex;
<HTML>
<!-- file created by $this_script -->
<head>
<meta HTTP-EQUIV=pragma CONTENT=no-cache>
<title>$hostname - Mailing List Interface</title>
<body>
<h1>$hostname - Browse [$listname] Archives</h1>
<hr>
<table cellpadding=0 cellspacing=0>
</table>
</body>
</HTML>

EOindex

    close(OUTPUT);
}

##
# Prcocess month/year - sanity checks and transformations
sub proc_y_m
{

    my (%month_array) = (
			 1  => "January",
			 2  => "February",
			 3  => "March",
			 4  => "April",
			 5  => "May",
			 6  => "June",
			 7  => "July",
			 8  => "August",
			 9  => "September",
			 10 => "October",
			 11 => "November",
			 12 => "December",
			);

    $month = $month_array{$month} || $month;

    if ( $year !~ /\d\d\d\d/ )
    {
	&error("Need a four digit year - (2003)");
    }
}

##
# The usage error output
sub usage
{
    $this_spaces    = $this_script;
    $this_spaces    =~ s/\S/ /g;

    print "Usage: $this_script listname\n";
    print "     -OR-\n";
    print "Usage: $this_script -filename list.mbox\n";
    print "       $this_spaces -listname general -month number -year YYYY\n";
    exit(1);
}

##
# Print a generic error message with this script's name (for embedded debug)
sub error
{
    my ($err_string) = @_;

    die "<!> ERROR($this_script): $err_string \n";
}

##
# Get command-line arguments
sub get_args
{
  &NGetOpt (
	    "month=s",			# month argument
            "year=s",			# year  argument
	    "filename=s",		# mbox  filename
	    "listname=s",		# mailing-list name
	    "host=s",	    	        # hostname to use
	   ) || ( $? = 257, &error("Invalid argument") );
}
