#!/usr/bin/perl
# -*-Perl-*-

#---
# $Id$
#
# ------------
# Description:
# ------------
#  Generate Year/Month archives from an all encompassing stored mbox file.
#
# -----------------
# Revision Details:    (Updated by Revision Control System)
# -----------------
#  $Date$
#  $Author$
#  $Revision$
#  $Source$
#
#---

require "newgetopt.pl";

$this_script    = $0;
$this_script    =~ s|^.*/([^/]*)|$1|;

&get_args();

if ( $opt_help )
{
    &help;
}

$archive_prog	= "/home/arabeyes/bin/mail_archiver.pl";

# Set appropriate umask
umask(002);

# See if user gave us what we need
if ( !$opt_filename || !$opt_listname) { &usage(1) };

# Specify an array to be used later
%month_array	= (
		   "Jan"  => ["January", 1],
		   "Feb"  => ["February", 2],
		   "Mar"  => ["March", 3],
		   "Apr"  => ["April", 4],
		   "May"  => ["May", 5],
		   "Jun"  => ["June", 6],
		   "Jul"  => ["July", 7],
		   "Aug"  => ["August", 8],
		   "Sep"  => ["September", 9],
		   "Oct"  => ["October", 10],
		   "Nov"  => ["November", 11],
		   "Dec"  => ["December", 12],
		  );

print "<< * >> Using $this_script\n";

print " < * > Accessing $opt_filename for mail-list '$opt_listname'\n";
open (IN_FILE, "< $opt_filename") or &error("Can't open $opt_filename\n");

while (<IN_FILE>)
{

# From general-admin  Mon, 16 Jul 21:00:03 2001
# From Mohammed Sameer  Mon Aug 2 04:19:30 2001
# From elzubeir  Sun Aug  5 20:58:16 2001
# From general-admin  Thu Aug 16 04:31:15 2001

    if ( /^From .*\s+([^\d\s]+)\s+\d*\s*\d+:\d+:\d+\s+(\w+).*/ )
    {
	# See if we've crossed into a new month/year
	if ( ($1 ne $cur_month) || ($2 ne $cur_year) )
	{
	    # A quick sanity check (to solve this error, mod infile by hand)
	    &check_progress($1, $2, $cur_month, $cur_year);

	    $cur_month = $1;
	    $cur_year  = $2;

	    # Munge the month name appropriately
	    ($month_name, $month_num) = &mod_month($cur_month);

	    $fileout = "${month_name}_${cur_year}.mbox";
  	    close(OUT_FILE);
 	    open (OUT_FILE, "> $fileout") or &error("Can't open $fileout\n");

	    print "       -> Created $fileout \n";
	    push (@files_to_process, "$fileout:$month_num:$cur_year");
	}
    }
    print OUT_FILE;
}

close(OUT_FILE);
close(IN_FILE);

print " < * > Processing newly created files\n";
foreach $line (@files_to_process)
{
    ($month_file, $month, $year) = split(/:/, $line);
    $archive_options	= " -month $month \\
			    -year $year \\
			    -file $month_file \\
			    -listname $opt_listname";

    print "       -> Archiving $month.$year\n";
    system("$archive_prog $archive_options >> out.archiver 2>&1");

    if (! $opt_keep )
    {
	unlink($month_file);
    }
}

if (! $opt_keep )
{
    print " < * > Temporary files were removed (use -k to preserve)\n";
}
print "<< * >> Done.\n";
exit(0);

       ###        ###
######## Procedures ########
       ###        ###

##
# See if there are any anomalies
sub check_progress
{

    my ($new_mo,
	$new_yr,
	$old_mo,
	$old_yr)	= @_;

    if (
	($month_array{$new_mo}[1] < $month_array{$old_mo}[1]) &&
	($new_yr != $old_yr+1)
       )
    {
	&error("Non-contiguous month - $new_mo < $old_mo (year = $new_yr)");
    }

    if ( $new_yr < $old_yr )
    {
	&error("Non-contiguous year - $new_yr < $old_yr");
    }
}

##
# Translate 3-letter month setting to fullname month and number
sub mod_month
{

    my ($in_month)	= @_;

    $month_name = $month_array{$in_month}[0];
    $month_num  = $month_array{$in_month}[1];

    return ($month_name, $month_num);
}

##
# The usage error output
sub usage
{
    my($die_after) = @_;
    print "Usage: $this_script -filename list.mbox -listname general ";
    print "[-keep] [-help]\n";
    if ($die_after) { exit(1); }
}

##
# The help menu
sub help
{
    &usage(0);
    print <<EOHelp;

-> generate Year/Month archives of supplied mbox file

  Options:

    -filename mbox          : filename of mbox to utilize
    -listname mail-list     : name of mailing-list mbox belongs to
    [-keep]                 : Keep intermediary mbox files (Optional)
EOHelp
    exit(1);

}

##
# Print a generic error message with this script's name (for embedded debug)
sub error
{
    my ($err_string) = @_;

    die "<!> ERROR($this_script): $err_string";
}

##
# Get command-line arguments
sub get_args
{
  &NGetOpt (
	    "help",			# help
	    "filename=s",		# mbox	  filename
	    "listname=s",		# mailing listname
	    "keep",			# keep temp. mbox files ?
	   ) || ( $? = 257, &error("Invalid argument\n") );
}
