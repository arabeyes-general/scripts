#!/usr/bin/perl
# -*-Perl-*-

#---
# $Id$
#
# ------------
# Description:
# ------------
#  Convert a Maildir format directory to an mbox file.
#
# -----------------
# Revision Details:    (Updated by Revision Control System)
# -----------------
#  $Date$
#  $Author$
#  $Revision$
#  $Source$
#
#---

require "ctime.pl";
require "newgetopt.pl";

##
# Keep track of run-time
$global_start_time      = time;

# Get the name of this script
$this_script    = $0;
$this_script    =~ s|^.*/([^/]*)|$1|;

# Set appropriate umask
umask(022);

# Process the command line
&get_args();

# Tend to user's options
if ($opt_help) { &help; }

$my_dir		= $opt_dir	|| "./";
$output_file    = $opt_output	|| "NEW_mbox.outfile";
$from_string	= $opt_string	|| "LIST-ADMIN-HERE";

# Some variable inits
$num_files	= 0;
$num_processed	= 0;

# Inform user what we're doing
print "<< * >> Using $this_script\n";
print " < - > Processing directory  - '$my_dir'\n";
if (!$opt_string)
{
    print " < - > Using the default $from_string as recipient string.\n";
}

# Read the contents of the directory at hand
if ( opendir(MY_DIR, $my_dir) )
{
    @files_in_dir  = grep(!/^\./, readdir MY_DIR);
    closedir(MY_DIR);
}

# Process each file within the directory read and generate output
open (OUTPUT, ">> $output_file");
foreach $file (sort (@files_in_dir))
{
    $num_files++;
    # Process only file numbers
    if ($file =~ /^\d+/)
    {
	$num_processed++;
	&process_file($my_dir, $file);
    }
}
close(OUTPUT);

# Inform user what we've accomplished
print " < + > Total converted files - $num_processed\n";
print " < + > Total processed files - $num_files\n";
print " < * > Generated => '$output_file' output mbox file.\n";

# Inform user how long the script ran for
&report_run_time($global_start_time);

exit(0);

       ###        ###
######## Procedures ########
       ###        ###

##
# Process the various files within read directory
sub process_file
{
    my($name_dir,
       $name_file)	= @_;

    # Local variables
    my (
	$full_filename,
	@file_lines,
	@header,
	$within_received,
	$rec_line,
       );

    # Strip-off the last slash (if any)
    $name_dir		=~ s/\/$//;
    $full_filename	= "$name_dir/$name_file";

    # Read-in the file
    open (IN_FILE, "< $full_filename") or die "Can't open $full_filename\n";
    while (<IN_FILE>)
    {
	# Skip some lines that shouldn't have been there to begin with
	if (
	    /^Return-Path: /	||
	    /^Mailing-List: /	||
	    /^Delivered-To: /
	   )
	{
	    next;
	}

	# Store off the file's contents (to be reprinted later)
	push (@file_lines, $_);

	# Deal with the multi-line Received: lines
	if ($within_received)
	{
	    # Remove the carriage returns
	    chomp;
	    # Stop the madness at the next header entry
	    if (/^\S+: \S+/)
	    {
		push (@header, $rec_line);
		$within_received = 0;
		undef($rec_line);
	    }
	    else
	    {
		# Continue accumulating lines (not done just yet)
		$rec_line .= $_;
	    }
	}
	    
	# Once I get a Received header entry, assume it will be multi-line
	if (/^Received: /)
	{
	    # Remove the carriage returns
	    chomp;
	    # Store off the line
	    $rec_line = $_;
	    $within_received = 1;
	}
       
    }
    close (IN_FILE);

    # All files are at hand and have the received headers; process 'em
    &process_n_print($full_filename, \@header, \@file_lines);
}

##
# Process file with both its contents and received header lines at hand
sub process_n_print
{
    my ($filename,
	$ref_header_lines,
	$ref_file_lines)	= @_;

    my @header_lines	= @{$ref_header_lines};
    my @file_lines	= @{$ref_file_lines};

    my (%assoc_month)	= (
                           "Jan"  => 1,
                           "Feb"  => 2,
                           "Mar"  => 3,
                           "Apr"  => 4,
                           "May"  => 5,
                           "Jun"  => 6,
                           "Jul"  => 7,
                           "Aug"  => 8,
                           "Sep"  => 9,
                           "Oct"  => 10,
                           "Nov"  => 11,
                           "Dec"  => 12,
                          );

    my (
	$done_one,
	$head,
	@line_contents,
	$day_str,
	$day_num,
	$month,
	$year,
	$time,
	$err_string,
	$entry,
       );

    # Sample Lines --
    #   Wed, 8 Aug 2001 08:59:38 -0700
    #   Sun, 15 Dec 2002 02:25:36 -0600 (CST)
    #   18 May 2003 12:12:58 +0200
    # Result --> From developer-admin  Wed Aug  8 11:04:12 2001
    $done_one = 0;
    foreach $head (@header_lines)
    {
	# Obtain the time/date contents of the received header
	@line_contents = split(/\;/, $head);

	# Decipher the string (examples above)
	if ( $line_contents[1] =~
	     /\s+([\D]*)[,\s+]*(\d+)\s+(\D+)\s+(\d+).*(\d\d:\d\d:\d\d)/ )
	{
	    $day_str	= $1;
	    # Grab the first found one only
	    if (!$done_one)
	    {
		$day_num	= $2;
		$month		= $3;
		$year		= $4;
		$time		= $5;
	    }

	    $day_str	=~ s/\,*\s+//i;
	    $done_one 	= 1;

	    # So some error checking (in case there is brain deadness)
	    # - maildir should be in order, but you never know...
	    if ($assoc_month{$glb_last_month} > $assoc_month{$month} )
	    {
		$err_string = "$glb_last_month > $month (in $filename)";
		die " < ! > ERROR - Month Misorder -- $err_string\n";
	    }
	    $glb_last_month = $month;
	}
    }

    # Sometimes the day string is not within the header - note it in case
    if (!$day_str)
    {
	print " < * > NOTE - Can't get 'Day' string in $filename\n";
    }

    # Produce the required output
    printf OUTPUT ("From $from_string  $day_str $month %2d $time $year\n",
		   $day_num);

    # Print out the original contents of the email files
    foreach $entry (@file_lines)
    {
	print OUTPUT $entry;
    }
    print OUTPUT "\n";
}

##
# Report the total run-time of the script
sub report_run_time
{
    my ($begin_time)    = @_;
    my (
        $end_time,
        $total_time
       );

    $end_time	= time;
    $total_time = $end_time - $begin_time;
  TIME_CASE:
    {
        # Hours
        if (($total_time/3600) > 1)
        {
            printf "<< * >> Run-time = %.2f hours\n",($total_time/3600);
            last TIME_CASE;
        }
        # Minutes
        if (($total_time/60) > 1)
        {
            printf "<< * >> Run-time = %.2f minutes\n",($total_time/60);
            last TIME_CASE;
        }
        # Seconds
        print "<< * >> Run-time = $total_time seconds\n";
    }
}

#
# Display this script's usage
sub help
{
    print <<EOHelp;

-> generate an mbox file out of a maildir directory

  Options:

    [-dir dirname]	: Specify a maildir directory to process
    [-output filename]	: Specify a an output file name to generate (mbox)
    [-string from_name]	: Specify the string to use as mail MTA

(*) All < > options are mandatory, all [ ] are optional.

EOHelp

    exit(1);
}

##
# {{{ Get the command line arguments
sub get_args
{
  &NGetOpt ("help",			# Help output
	    "dir=s",			# Directory name
            "output=s",			# Output file name
            "string=s",			# String to use for List-admin
           ) || ( $? = 257, die "Invalid argument\n" );
}
