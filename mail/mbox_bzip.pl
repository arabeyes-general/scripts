#!/usr/bin/perl
# -*-Perl-*-

#---
# $Id$
#
# ------------
# Description:
# ------------
#  This script is to archive the various mailing-list mbox files by
#  attaching today's date to them and then by running 'bzip2' on them.
#
#  The script then creates an empty shell of a file to be used as
#  though nothing happened.
#
#  The files of concern are in,
#    /var/lib/mailman/archives/private/*.mbox/*
#
# -----------------
# Revision Details:    (Updated by Revision Control System)
# -----------------
#  $Date$
#  $Author$
#  $Revision$
#  $Source$
#
# (www.arabeyes.org - under GPL license - by Nadim Shaikli)
#---

require "ctime.pl";
require "newgetopt.pl";

use English;
use File::Find;

##
# Keep track of run-time
$glb_start_time	= time;

##
# Specify global variable values and init some Variables
$this_script    = $0;
$this_script    =~ s|^.*/([^/]*)|$1|;

##
# Find how many spaces the scripts name takes up (nicety)
$this_spaces    = $this_script;
$this_spaces    =~ s/\S/ /g;

chop($date = `date +%G%m%d`);
chop($user = `whoami`);

#
# Process the command line
&get_args();

if ( $opt_help ) { &help; }

# Only the mailing-list user is to run this (safer on file permissions)
if ( ($user ne "list") & !$opt_test )
{
    die "<<!>> ERROR($this_script): User 'list' must run this script.\n";
}

# Set appropriate umask
umask(002);

# Get user specified options
$proj_dir	= $opt_dir	|| &usage(1);

print "<< * >> Using $this_script\n";

# Process directory with to collect files of interest into an array
find(\&get_files, $proj_dir);

# Make sure we have something to work on
if (! defined %assoc_files)
{
    die "<<!>> ERROR($this_script): NO mbox files found.\n";
}

# Process the files found
foreach my $file (sort (keys %assoc_files))
{
    print " < * > Processing '$file' \n";
    if ( !$opt_test )
    {
	rename($file, "$file.$date");
	system("bzip2 --best $file.$date");
	system("touch $file");
    }
    else
    {
	print "(dbg) - mv $file $file.$date \n";
	print "(dbg) - bzip2 --best $file.$date \n";
	print "(dbg) - touch $file \n";
    }
}

# Tell 'em how long this process took
&report_run_time($glb_start_time);

exit(0);

       ###        ###
######## Procedures ########
       ###        ###

##
# Gather/capture all files of concern
sub get_files
{
    if ( ($File::Find::name =~ /\.mbox$/) )
    {
	# Make sure what we fell upon is a file (not a dir)
	if (-f $_)
	{
	    $assoc_files{$File::Find::name} = $_;
	}
    }

}

##
# Report the total run-time of the script
sub report_run_time
{
    my ($begin_time)    = @_;
    my (
        $end_time,
        $total_time
       );

    $end_time = time;
    $total_time = $end_time - $begin_time;
  TIME_CASE:
    {
        # Hours
        if (($total_time/3600) > 1)
        {
            printf "<< * >> Run-time = %.2f hours\n",($total_time/3600);
            last TIME_CASE;
        }
        # Minutes
        if (($total_time/60) > 1)
        {
            printf "<< * >> Run-time = %.2f minutes\n",($total_time/60);
            last TIME_CASE;
        }
        # Seconds
        print "<< * >> Run-time = $total_time seconds\n";
    }
}

##
# Print short usage info
sub usage
{
    my ($die_after,
	$err_msg)	= @_;

    if (defined $err_msg)
    {
	print $err_msg;
    }

    print qq
|Usage: $this_script <-dir root_file_path> 
|;

    if ( $die_after ) { exit(5); }
}

##
# Print one-liner help
sub help
{
    &usage(0);

    print qq|
-> Tag using today\'s date then bzip all found mbox files

  Options:

    <-dir root_path>  : Specify root directory of where files reside

|;
    exit(10);
}

##
# Get the command line arguments
sub get_args
{
  &NGetOpt (
	    "help",		# print a nice help screen
	    "dir=s",		# note the dir to search for files in
	    "test",		# test mode - no changes take place
	   ) || ( $? = 257, &usage(1, "Invalid argument\n") );
}
