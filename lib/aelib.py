#! /usr/bin/python
#---
# $Id$
#
# ------------
# Description:
# ------------
#  This module regroups a few functions common to other admin scripts,
#  mainly database related functions.
#
# -----------------
# Revision Details:    (Updated by Revision Control System)
# -----------------
#  $Date$
#  $Author$
#  $Revision$
#  $Source$
#---

#Importing Modules

import MySQLdb
import re

#Variables Definition

DBHost = ""
DBName = ""
DBUser = ""
DBPass = ""

SupportEmail = ""

CvsRoot = "/home/arabeyes/cvs/" # Keep the '/' at the end 

ProjectsPage = "http://www.arabeyes.org/project.php"
PeoplePage   = "http://www.arabeyes.org/people.php"

#Functions Definition

def ProjectId2Name(ProjectId):
	DBCursor.execute("select proj_name from proj_about where proj_id='%s'" % (ProjectId))
	
	QueryResult = DBCursor.fetchone()
	
	if(not QueryResult):
		return "Unknown project"
	else:
		return QueryResult[0]

def ProjectName2Id(ProjectName):
	DBCursor.execute("select proj_id from proj_about where proj_name='%s'" % (ProjectName))
	
	QueryResult = DBCursor.fetchone()
	
	if(not QueryResult):
		return -1
	else:
		return QueryResult[0]

def ProjectName2Url(ProjectName):
	return "%s?proj=%s" % (ProjectsPage, ProjectName)

def UserId2Name(UserId):
	DBCursor.execute("select username from user where id='%s'" % (UserId))
	
	QueryResult = DBCursor.fetchone()
	
	if(not QueryResult):
		return "Unknown user"
	else:
		return QueryResult[0]

def UserName2Id(UserName):
	DBCursor.execute("select id from user where username='%s'" % (UserName))
	
	QueryResult = DBCursor.fetchone()
	
	if(not QueryResult):
        	return -1
	else:
        	return QueryResult[0]

def UserId2RealName(UserId):
	DBCursor.execute("select fname,lname from user where id='%s'" % (UserId))
	
	QueryResult = DBCursor.fetchone()
	
	if(not QueryResult):
		return "Unknown user"
	else:
		return "%s %s" % (QueryResult[0], QueryResult[1])

def UserName2Url(UserName):
	return "%s?username=%s" % (PeoplePage, UserName)

def GetUserLastCommit(UserId):
        DBCursor.execute('select UNIX_TIMESTAMP(logdate) from cvs where userid=%s order by logdate desc' % (UserId))
        
	QueryResult = DBCursor.fetchone()
        
	if(not QueryResult):
                return 0
	
        return QueryResult[0]

def CvsLog2ProjectName(CvsLog):
	ProjectMatch = re.search("Update of %sprojects\/external\/([^\/\s]+)[\/|/\s]" % (CvsRoot), CvsLog) #Needs to be before 'internal' projects
	if(ProjectMatch):
		return ProjectMatch.group(1)
	
	ProjectMatch = re.search("Update of %sprojects\/([^\/\s]+)[\/|\s]" % (CvsRoot), CvsLog)
	if(ProjectMatch):
		return ProjectMatch.group(1)
	
	ProjectMatch = re.search("Update of %sart\/([^\/\s]+)[\/|\s]" % (CvsRoot), CvsLog)
	if(ProjectMatch):
		return ProjectMatch.group(1)
	
	ProjectMatch = re.search("Update of %stranslate\/([^\/\s]+)[\/|\s]" % (CvsRoot), CvsLog)
	if(ProjectMatch):
		return ProjectMatch.group(1)
	
	return "Unknown project"

# Main Program --- Database Connection

DB = MySQLdb.connect(DBHost, DBUser, DBPass, DBName)

DBCursor = DB.cursor()
