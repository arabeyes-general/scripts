#!/usr/bin/perl
# -*-Perl-*-

#---
# $Id$
#
# ------------
# Description:
# ------------
#  Remove deep hierarchy from a directory tree and copy its new renamed
#  content (each dir hierarchy get a "." name).  With a command-line
#  option, the verse process is available (ie. to reintroduce that
#  hierarchy back).
#
# -----------------
# Revision Details:    (Updated by Revision Control System)
# -----------------
#  $Date$
#  $Author$
#  $Revision$
#  $Source$
#
# (www.arabeyes.org - under GPL license)
#---

require "ctime.pl";
require "newgetopt.pl";

use English;
use File::Find;
use File::Path;
use File::Copy;

##
# Keep track of run-time
$global_start_time	= time;

##
# Specify global variable values and init some Variables
$this_script	= $0;
$this_script    =~ s|^.*/([^/]*)|$1|;

##
# Find how many spaces the scripts name takes up (nicety)
$this_spaces    = $this_script;
$this_spaces    =~ s/\S/ /g;

chop($pwd = `pwd`);

# Process the command line
&get_args();

if ( $opt_help ) { &help; }

print "<< * >> Using $this_script\n";

# Process user command-line inputs (if any)
$proj_dir	= $opt_from	|| &usage(1);
$prefix 	= $opt_to	|| "/tmp";
$gen_hier	= $opt_gen_hier;

# Set some default values
$po_ext		= "po";

# Specific dirs to remove/add per tree (uselessites :-)
%special_dirs	= (
		   "stage1"	=> "\/debian\/po",
		  );

# If we're dealing with relative paths, absolute 'em !!
if ( $proj_dir !~ /^\// )
{
    $proj_dir	= "$pwd/$proj_dir";
}
if ( $prefix !~ /^\// )
{
    $prefix	= "$pwd/$prefix";
}

# Find the appropriate dirs/files and process them
find(\&proc_files, $proj_dir);

# Report back some results
foreach $key (sort (keys %stat))
{
    # Set to zero if none were renamed (nicer looking than empty :-)
    $stat{$key}{"renamed"} = $stat{$key}{"renamed"} || 0; 
    printf (" < * > Stat (%10s) - %5u Renamed, %5u Copied\n",
	    $key, $stat{$key}{"renamed"}, $stat{$key}{"total"});
}

# Report how long this whole thing took
&report_run_time($global_start_time);

exit(0);

       ###        ###
######## Procedures ########
       ###        ###

##
# Process entire tree for renaming and copying
sub proc_files
{
    # Some local variables
    my (
	$special,
	$my_tree,
	$info_str
       );

    # Deal only with files with appropriate file extension
    if ( ($File::Find::name =~ /\.$po_ext/) )
    {
	# Grab filename (ie. one.two.three.my_file.po)
	$filename	= $_;

	# Degenerate file into its collective pieces
	if ( $filename =~ /(.*)\.($po_ext[t]*[,v]*$)/ )
	{
	    $my_file	= $1;
	    $my_ext	= $2;
	}

	# Remember where that file came from (ie. which dir)
	$dir_now	= $File::Find::dir;

	($dir_root,
	 $dir_rest)	= &dissect_path_file($dir_now, $proj_dir);

	# Deal only with offending hierarchically deep dirs
        @dirs	= (split(/\//, $File::Find::dir));
	if ($#dirs > 1 && !$gen_hier)
	{
	    # Print this message once
	    if (!$done_this)
	    {
		print " < * > Remove Hierarchy Mode\n";
		$done_this = 1;
	    }

	    # Deal with the appropriate offending dirs
	    foreach $root_dir (sort (keys %special_dirs))
	    {
		# Dealing with something I need to rename
		if ( ($dir_now =~ /(^.*$root_dir)/)	&&
		     ($dir_now =~ /$special_dirs{$root_dir}/) )
		{
		    # Note that we've done a special dir
		    $special	= 1;

		    # Generate a modified string of dir's name (strip off things I don't need)
		    $dir_rest	=~ s/$special_dirs{$root_dir}//g;

		    # See if there are entire to be renamed
		    if (defined $dir_rest)
		    {
			$info_str	= "& Renaming ";

			# Keep track of renamed statistics
			$stat{$dir_root}{"renamed"} += 1;
		    }
 		    else
 		    {
 			# Deal with no hierarchy situations
 			$dir_rest	.= "/$my_file";
 		    }

		    # Keep track of total statistics
		    $stat{$dir_root}{"total"} += 1;
		}
	    }

	    $dir_rest	=~ s/\//./g;

	    # Deal with dirs of no concern to us (not noted in special_dirs)
	    if (!$special)
	    {
		# Store-off any directory-hierarchy
		$my_tree	= ( $dir_rest ? $dir_rest : "" );
		$dir_rest	= $my_file;

		# Keep track of total statistics
		$stat{$dir_root}{"total"} += 1;
	    }

	    # Get rid of that nasty hierarchy ;-)
	    # Copy the file over (renamed or not)
  	    &copy_it("$info_str",
  		     "$dir_now/$filename",
  		     "$prefix/$dir_root/$my_tree",
  		     "$dir_rest.$my_ext");
        }

	# Deal with the upload back to mothership situation (recreate hierarchy - ie. ugly-up)
	elsif ($#dirs > 1)
	{
	    # Print this message once
	    if (!$done_this)
	    {
		print " < * > Generate Hierarchy Mode\n";
		$done_this = 1;
	    }

 	    # Specify a modified file variable
 	    $file_mod	= $my_file;

	    # Deal with the appropriate offending dirs
	    foreach $root_dir (keys %special_dirs)
	    {
		# Dealing with something I need to rename
		if ( ($dir_now =~ /(^.*$root_dir)/) &&
		     (!$dir_rest) )
		{
		    # Note that we've done a special dir
		    $special	= 1;

		    # Split off based on alleged hierarchy
		    @my_hier	= split(/\./, $my_file);

		    # Make sure we have something to work with here
		    if ($#my_hier > 1)
		    {
			$info_str	= "& Renaming ";

			# Add back the stripped off dirs (uselessites)
			$file_mod	=~ s/$/$special_dirs{$root_dir}/g;

			# Regenrate that nasty hierarchy ;-)
			$file_mod	=~ s/\./\//g;

			# Note new filename
			$my_file	= "ar";

			# Keep track of renamed statistics
			$stat{$dir_root}{"renamed"} += 1;
		    }
		    else
		    {
			# If there was no hierarchy, remove filenames from variable
			$file_mod	=~ s/$my_file$//;
		    }

		    # Keep track of total statistics
		    $stat{$dir_root}{"total"} += 1;
		}
	    }

	    if (!$special)
	    {
		$dir_rest	.= ( $dir_rest ? "/" : "" );
		$file_mod	=~ s/$my_file$//;

		# Keep track of total statistics
		$stat{$dir_root}{"total"} += 1;
	    }

	    # Copy the file over (renamed or not)
	    &copy_it($info_str,
		     "$dir_now/$filename",
		     "$prefix/$dir_root/$dir_rest$file_mod",
		     "$my_file.$my_ext");
	}
    }
    # Copy over (without modifications) files of no concern (ie. none .po*)
    else
    {
	$file		= $_;
	$dir_frm	= $File::Find::name;
	$dir_to		= $File::Find::name;
	$dir_to		=~ s/$proj_dir//;
	$dir_to		=~ s/$file//;
	$dir_to		=~ s/^\///;

	# Grab all appropriate files (except for CVS dirs)
	if ( (-f $dir_frm) 		&&
	     ($dir_frm =~ /$file/)	&&
	     ($dir_frm !~ /CVS/) )
	{
	    &copy_it("",
		     "$dir_frm",
		     "$prefix/$dir_to",
		     "$file");

	    # Keep track of total statistics
	    $stat{$dir_to}{"total"} += 1;

	}
    }
}

##
# Determine the root and child paths
sub dissect_path_file
{
    my ($dir_path,
	$cur_path)	= @_;

    $dir_path	=~ s/$cur_path//;
    $dir_path	=~ s/^\///;
    @dir_arr	= split(/\//, $dir_path);
    $root_path	= $dir_arr[0];
    $dir_path	=~ s/$root_path//;
    $dir_path	=~ s/^\///;

    return($root_path, $dir_path);

}

##
# Do the proper dir creation and copying
sub copy_it
{
    my($cp_str,
       $cp_frm,
       $cp_to_path,
       $cp_to_filename)		= @_;

    # Get rid of any trailing slashes
    if ( $cp_to_path =~ /\/$/ )
    {
	chop($cp_to_path);
    }

    # Make sure we have a dir to copy into; if not, create it
    if (! -d "$cp_to_path" )
    {
	# Directory doesn't exist - create new year/month & index file
	print " < ! > New directory created - $cp_to_path \n";
	mkpath("$cp_to_path", 0, 0755) or die "<<!>> Can't create $cp_to_path";
#	system "mkdir -p $cp_to_path" || die "<<!>> Can't create $cp_to_path";
    }
   
    # Do the actual copy
    print " < + > Copying $cp_str ...\n";
    print "  <->  From - $cp_frm\n";
    print "  <->  To   - $cp_to_path/$cp_to_filename\n";

    if ( !(copy ("$cp_frm",
		 "$cp_to_path/$cp_to_filename")) )
    {
	die "<<!>> ERROR($this_script): Can't copy $cp_frm : $!\n";
    }
}

##
# Report the total run-time of the script
sub report_run_time
{
    my ($begin_time)    = @_;
    my (
        $end_time,
        $total_time
       );

    $end_time = time;
    $total_time = $end_time - $begin_time;
  TIME_CASE:
    {
        # Hours
        if (($total_time/3600) > 1)
        {
            printf "<< * >> Run-time = %.2f hours\n",($total_time/3600);
            last TIME_CASE;
        }
        # Minutes
        if (($total_time/60) > 1)
        {
            printf "<< * >> Run-time = %.2f minutes\n",($total_time/60);
            last TIME_CASE;
        }
        # Seconds
        print "<< * >> Run-time = $total_time seconds\n";
    }
}

##
# Print short usage info
sub usage
{
    my ($die_after) = @_;

    print qq
|Usage: $this_script <-from dir_from> <-to dir_to> [-gen_hier]
|;
#       $this_spaces [-gen_hier]

    if ( $die_after ) { exit(5); }
}

##
# Print one-liner help
sub help
{
    &usage(0);

    print qq|
-> remove hierarchy (or generate it) in regards to a translation project

  Options:

    <-from dir_from> : Specify source      dir to attain files from
    <-to dir_to>     : Specify destination dir to write  files to [/tmp]
    [-gen_hier]      : Specify to regenerate hierarchy

|;
    exit(10);
}

##
# Get the command line arguments
sub get_args
{
  &NGetOpt (
            "help",                     # Print a nice help screen
            "from=s",                   # Indicate source dir
	    "to=s",			# Indicate destination dir
	    "gen_hier",			# Indicate to re-generate hierarchy
           ) || ( $? = 257, die "Invalid argument\n" );
}
