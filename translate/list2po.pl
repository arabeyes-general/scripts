#!/usr/bin/perl
# -*-Perl-*-

#---
# $Id$
#
# ------------
# Description:
# ------------
#  Generate (or combine into) a gettext .PO file from a list of terms.
#
#  - Read-in a file which lists a single term per line
#  - Read-in a .PO (if prompted) to combine into
#  - Generate an alphabetically unique list of terms
#    while maintain existing .PO file's contents.
#
# -----------------
# Revision Details:    (Updated by Revision Control System)
# -----------------
#  $Date$
#  $Author$
#  $Revision$
#  $Source$
#
# (www.arabeyes.org - under GPL license - by Nadim Shaikli)
#---

require "ctime.pl";
require "newgetopt.pl";

use utf8;
use English;

##
# Keep track of run-time
$global_start_time      = time;
@inputs                 = @ARGV;

##
# Specify global variable values and init some Variables
$this_script            = $0;
$this_script            =~ s|^.*/([^/]*)|$1|;

##
# Find how many spaces the scripts name takes up (nicety)
$this_spaces            = $this_script;
$this_spaces            =~ s/\S/ /g;

# Process the command line
&get_args();

if ( $opt_help ) { &help; }

if ( !$opt_list )
{
#    print "<<!>> ERROR($this_script): '-list' option needed ! \n";
    &usage(1);
}

print "<< * >> Using $this_script\n";

$default_out		= ($opt_po ? "$opt_po.out" : "$opt_list.out");
$out_file		= ($opt_output || $default_out);

# Do actual file processing and manipulation
if (-e $opt_po)
{
    ($ref_preamble,
     $ref_terms_in)	= &process_pofile($opt_po);
}
$ref_terms_listed	= &process_listfile($opt_list, $ref_terms_in);
%terms_listed		= %{$ref_terms_listed};

# Dump out any warnings we might have encountered
if ( @WARN )
{
    $warn = $#WARN + 1;
    print " < ! > There were $warn warnings generated (check output file)\n";
}

# Make sure we have something to output
if ( defined(%terms_listed) )
{
    &generate_output($out_file, $ref_preamble, \%terms_listed);
    print " < * > Generated '$out_file' output file\n";
}

# Tell 'em how long the whole we were ON !!
&report_run_time($global_start_time);

exit(0);

       ###        ###
######## Procedures ########
       ###        ###

##
# Process gettext PO file (read it in and store off the info)
sub process_pofile
{
    my ($po_file)	= @_;

    my(
       $preamble,
       @approvers,
       @rejectors,
       %termlist,
       $other_name,
       $line,
      );

    open (PO_FILE, "< $po_file") or die "Can't open $po_file - $!\n";

    while (<PO_FILE>)
    {
	if (/^\# WARNING/ || /^\#\,\s*fuzzy/i)
	{
	    # Skip over regenerative indicators
	    next;
	}

	if (/^\#\s*ApprovedBy:/)
	{
	    $cur_line = $_;
	    $cur_line =~ s/\#\s*//;
	    # See how many people YAY'ed
	    @approvers	= split(/\s+/, $cur_line);

	    # Save off any pre-existing header info to be regurgitated later
	    if ( !defined($preamble) )
	    {
		$preamble = $line;
	    }
	    undef($line);
	}

	if (/^\#\s*RejectedBy:/)
	{
	    $cur_line = $_;
	    $cur_line =~ s/\#\s*//;
	    # See how many people NAY'ed
	    @rejectors	= split(/\s+/, $cur_line);
	}

	# Once the term is reached process data segment
        if (/msgid\s+\"(\S+)\"/)
        {
	    # NOTE: Duplicates are not checked for in the PO file !!

	    # Save data off (lower_case => actual_case/data)
	    $termlist{"\L$1\E"}{name}	= $1;
	    $termlist{"\L$1\E"}{pre}	= $line;
	    # Get next line (the msgstr)
	    $termlist{"\L$1\E"}{post}	= <PO_FILE>;
	    $termlist{"\L$1\E"}{ok}	= $#approvers;
	    $termlist{"\L$1\E"}{bad}	= $#rejectors;
	    # Clear out what I've been saving
	    undef($line);
	    undef(@approvers);
	    undef(@rejectors);
	}
	$line .= $_;
    }
    close (PO_FILE);

    return(\$preamble, \%termlist);
}

##
# Process the input list of terms to process and combine into gettext PO
sub process_listfile
{
    my ($list_file,
	$ref_termlist)	= @_;

    my(
       %termlist,
       %interms,
       $dup_ok,
       $entry,
      );

    %termlist	= %{$ref_termlist};

    open (LIST_FILE, "< $list_file") or die "Can't open $list_file - $!\n";

    while (<LIST_FILE>)
    {
	# Skip over any comments or empty lines within file
	if (/^\#/ || /^\s*$/)
	{
	    print "SKIPPING |$_|";
	    next;
	}

	# Get rid of any leading/trailing spaces
	s/^\s*//g;
	s/\s*$//g;
	chomp;

	# See if we are dealing with a legal/legit/OK'ed duplicate
	if (/\# Duplicate OK/i)
	{
	    $dup_ok = 1;
	}

	# Duplicates are very frowned upon, if you see 'em scream and run
	if ( $interms{"\L$_\E"} )
	{
	    $other_name = $interms{"\L$_\E"}{name};
	    push (@WARN, "LIST Duplicates '$_' - '$other_name'\n");
	    $interms{"\L$_\E"}{name} = 0;
	    next;
	}

	# Time to store our data
	if ( $dup_ok )
	{
	    # Get rid of duplicate OK indicator
	    @line = split(/\s*\#/, $_);

	    # Save data off locally (actual_case => actual_case)
	    $interms{$line[0]}{name} = $line[0];
	    undef($dup_ok);
	}
	else
	{
	    # Save data off locally (lower_case => actual_case)
	    $interms{"\L$_\E"}{name} = $_;
	}
    }
    close (LIST_FILE);

    # See if we have any duplication
    foreach $entry (keys %interms)
    {
	# See if the entry is valid and if it doesn't already exist
	if ( $interms{$entry}{name} &&
	     !(defined($termlist{$entry}{name})) )
	{
	    # Add it to our global hash
	    $termlist{$entry}{name}	= $interms{$entry}{name};
	}
    }

    return(\%termlist);
}

##
# Process out data and dump out the results
sub generate_output
{
    my ($outfile,
	$ref_preamble,
	$ref_termlist)	= @_;

    my (
	$preamble,
	%termlist,
	$entry,
       );

    $preamble = ${$ref_preamble};
    %termlist = %{$ref_termlist};

    open (OUTPUT, "> $outfile") or die "Can't open $outfile - $!\n";

    # Print out any warnings to the output file
    if ( defined(@WARN) )
    {
	foreach $entry (@WARN)
	{
	    print OUTPUT "# WARNING($this_script): $entry";
	}
    }

    # Regurgitate else print out generic gettext PO header info
    if ( defined($preamble) )
    {
	print OUTPUT $preamble;
    }
    else
    {
	&gen_output_head(\*OUTPUT);
    }

    # Sort on the finalized term listings in hash
    foreach $entry (sort {lc($termlist{$a}{name}) cmp lc($termlist{$b}{name})}
		    keys %termlist)
    {
	# Print any pre-term sub-heading info
	if ( $termlist{$entry}{pre} )
	{
	    print OUTPUT $termlist{$entry}{pre};
	}
	else
	{
	    print OUTPUT "# ApprovedBy:\n";
	    print OUTPUT "# RejectedBy:\n";
	}		

	# Note term's status based on QAC member votes
	if ( ($termlist{$entry}{bad} || $termlist{$entry}{ok}) &&
	     $termlist{$entry}{bad} >= $termlist{$entry}{ok} )
	{
	    print OUTPUT "#, fuzzy\n";
	}

	# Print the term (actual spelling)
	print OUTPUT "msgid \"$termlist{$entry}{name}\"\n";

	# Print translation section
	if ( $termlist{$entry}{post} )
	{
	    print OUTPUT $termlist{$entry}{post};
	}
	else
	{
	    print OUTPUT "msgstr \"\"\n";
	}
	print OUTPUT "\n";
    }

    close (OUTPUT);
}

##
# Print out the .po header
sub gen_output_head
{
    my ($OUTF)    = @_;
    
    print $OUTF qq|# Translation of $out_file to Arabic
#-*-
# --
# \$Id\$
# --
#
# This is a script generated file (by '$this_script')
#
# The file is the result of processing a list of terms
# for inspection by Arabeyes' Quality Assurance Commitee
# (QAC).  The intent is for this file to be a technical
# dictionary to serve the needs of technical translators.
#
# - Copyright (C) 2005 Free Software Foundation, Inc.
#
# To generate a simple equality list, run this command-line
#   perl -n -e 'if (/^msgid\s+"(.*)"/)  { print "$1 = "; } \
#               if (/^msgstr\s+"(.*)"/) { print "$1\\n"; }' \
#               this_file_name_here
#
#-*-
msgid ""
msgstr ""
"Project-Id-Version: $out_file\\n"
"POT-Creation-Date: 2005-01-20 18:00-0800\\n"
"PO-Revision-Date : 2005-01-21 11:15-0800\\n"
"Last-Translator: You Here <you\@email.org>\\n"
"Language-Team: Arabic <doc\@arabeyes.org>\\n"
"MIME-Version: 1.0\\n"
"Content-Type: text/plain; charset=UTF-8\\n"
"Content-Transfer-Encoding: 8bit\\n"
"X-Generator: KBabel 1.9\\n"

|;
}

##
# Report the total run-time of the script
sub report_run_time
{
    my ($begin_time)    = @_;
    my (
        $end_time,
        $total_time
       );

    $end_time = time;
    $total_time = $end_time - $begin_time;
  TIME_CASE:
    {
        # Hours
        if (($total_time/3600) > 1)
        {
            printf "<< * >> Run-time = %.2f hours\n",($total_time/3600);
            last TIME_CASE;
        }
        # Minutes
        if (($total_time/60) > 1)
        {
            printf "<< * >> Run-time = %.2f minutes\n",($total_time/60);
            last TIME_CASE;
        }
        # Seconds
        print "<< * >> Run-time = $total_time seconds\n";
    }
}

##
# Display this scripts usage (with or without a death afterwards)
sub usage
{
    my ($die_after)     = @_;

    print "Usage: $this_script <-list filename> [-po filename]\n";
    print "       $this_spaces                  [-output filename]\n";
    if ($die_after)
    {
        exit(1);
    }
}

##
# Display this scripts usage (with or without a death afterwards)
sub help
{
    &usage(0);
    print <<EOHelp;

-> generate a gettext PO file from a list of terms

  Options:

    <-list filename>   : Specify an input file with a list of terms
    [-po filename]     : Specify a gettext PO file to combine with
    [-output filename] : Specify output filename to utilize

(*) All < > options are mandatory, all [ ] are optional.

EOHelp

    exit(1);
}
##
# Get the command line arguments
sub get_args
{
  &NGetOpt (
            "help",		# print a nice help screen
            "debug",		# print various debug output while running
            "list=s",		# specify list filename
            "po=s",		# specify list filename
            "output=s",		# specify output filename
           ) || ( $? = 257, die "Invalid argument\n" );
}
