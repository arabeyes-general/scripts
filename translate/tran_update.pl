#!/usr/bin/perl
# -*-Perl-*-

#---
# $Id$
#
# ------------
# Description:
# ------------
#  A script to update and sync-up files with the mother-ship :-)
#
#  Procedure should include:
#  - cvs/download files from the MAIN source (mother-ship) into a dir (/tmp)
#  - Run SCRIPT with,
#    + -main noting the location of the newly   downloaded main  files
#    + -tran noting the location of the locally translated local files 
#  - What the SCRIPT does:
#    1. if    downloaded .pot and no local .po - copy .pot locally
#    2. if    downloaded .pot and    local .po [even if moved] - msgmerge
#    3. if NO downloaded .pot and    local .pot or .po - rename local files
#    4. if NO downloaded  dir and    local  dir - rename local dir
#  - Commit changes to cvs
#
# -----------------
# Revision Details:    (Updated by Revision Control System)
# -----------------
#  $Date$
#  $Author$
#  $Revision$
#  $Source$
#
# (www.arabeyes.org - under GPL license)
#---

require "newgetopt.pl";

use English;
use File::Find;
use File::Copy;

##
# Keep track of run-time
$start_time     = time;

##
# Specify global variable values and init some Variables
$this_script	= $0;
$this_script	=~ s|^.*/([^/]*)|$1|;
$version        = ' ($Revision$)';

##
# Find how many spaces the scripts name takes up (nicety)
$this_spaces    = $this_script;
$this_spaces    =~ s/\S/ /g;

chop($date = `date`);
chop($pwd  = `pwd`);

# Process the command line
&get_args();

if ( $opt_help ) { &help; }

# Make sure user entered required command-line options
if (!$opt_main_dir || !$opt_tran_dir)
{
    &usage(1);
}

# Global defines
$dir_main	= $opt_main_dir;
$dir_tran	= $opt_tran_dir;
$old_rename	= !$opt_keep_old_files || 0;

# See how to deal with in regards to the msgmerge log output
# - If no '-log' output is discarded, else a default name can be used
$merge_log      = defined($opt_log) ? ($opt_log || "msgmerge.log") : 
                                      "/dev/null";

# File extensions being dealt with
$ext_orig	= $opt_ext_main || "pot";
$ext_tran	= "po";

# Remove the last slash if there was one (breaks things)
$dir_main	=~ s|/$||;
$dir_tran	=~ s|/$||;

# If this is a relative path, absolute it !!
if ( $dir_main !~ /^\// )
{
    $dir_main	= "$pwd/$dir_main";
}
if ( $dir_tran !~ /^\// )
{
    $dir_tran	= "$pwd/$dir_tran";
}

if (!-d $dir_main )
{
    &err("$dir_main doesn't exist \n");
}
if (!-d $dir_tran )
{
    &err("$dir_tran doesn't exist \n");
}

# Let user know that script is running
print "<< * >> Using $this_script$version\n";

# Generate/Process directory with all the files (generate arrays)
find(\&proc_downloaded, $dir_main);
find(\&proc_local_orig, $dir_tran);
find(\&proc_local_tran, $dir_tran);

if (! defined %assoc_main_orig)
{
    &err("NO files with '$ext_orig' extension found in $dir_main\n");
}

# Cycle through all the mothership ("main") directories
foreach $dir (sort (keys %assoc_main_orig))
{
    foreach $file (sort (@{$assoc_main_orig{$dir}}))
    {
	# Make sure we have the same tree image as the mothership/main
	if (! -d "$dir_tran/$dir" )
	{
	    print "  <*> New directory created - $dir_tran/$dir \n";
	    mkdir("$dir_tran/$dir", 0755);
	}

	if (! $assoc_local_tran_found{"$dir$file"} )
	{
	    # Update only when file needs to be updated
	    if ( $assoc_local_orig_found{"$dir$file"} )
	    {
		my $diff = `diff $dir_main/$dir$file.$ext_orig \\
                            $dir_tran/$dir$file.$ext_orig`;
		if ( !$diff )
		{
		    next;
		}
	    }
	    # Make sure new file is indeed new and wasn't simply moved
	    print "  <!> New Orphan - $dir_main/$dir$file.$ext_orig \n";
	    if (defined($assoc_main_orphan{$file}))
	    {
		&err("multiple instances of main '$file.$ext_orig' \n");
	    }
	    $assoc_main_orphan{$file} =	"$dir_main/$dir$file.$ext_orig";
	}
	else
	{
 	    # If its in the mothership/main and its found translated, merge
	    do_the_merge(0,
			 "$dir$file",
			 "$dir_tran/$dir$file.$ext_tran",
			 "$dir_main/$dir$file.$ext_orig",
			 "$dir_tran/$dir$file");
	}
    }

    ##
    # Check if there are any outdated files.
    # - (have translated, but no downloaded orig or maybe its been moved)
    foreach $file (sort (@{$assoc_local_tran{$dir}}))
    {
	if (! $assoc_main_orig_found{"$dir$file"} )
	{
	    print "  <!> Translated Orphan - $dir_tran/$dir$file.$ext_tran \n";
	    if (defined($assoc_tran_orphan{$file}))
	    {
		&err("multiple instances of tran '$file.$ext_tran' \n");
	    }
	    $assoc_tran_orphan{$file} = "$dir_tran/$dir,$file.$ext_tran";
	}
    }

    # - (have un-translated, but no downloaded orig)
    foreach $file (sort (@{$assoc_local_orig{$dir}}))
    {
	if (! $assoc_main_orig_found{"$dir$file"} )
	{
	    my $src_obj = "$dir_tran/$dir$file.$ext_orig";
	    print "  <-> Outdated (renamed w/ REM_) - $src_obj \n";
	    rename("$dir_tran/$dir$file.$ext_orig",
		   "$dir_tran/${dir}REM_$file.$ext_orig");
	}
    }
}

# See if there are any files to capture as translated orphans in directories
# that are no longer relevant
foreach $dir (sort (keys %assoc_local_tran))
{
    if (! $assoc_main_orig{$dir} )
    {
	foreach $file (sort (@{$assoc_local_tran{$dir}}))
	{
	    print "  <!> Translated Orphan - $dir_tran/$dir$file.$ext_tran \n";
	    if (defined($assoc_tran_orphan{$file}))
	    {
		&err("multiple instances of tran '$file.$ext_tran' \n");
	    }
	    $assoc_tran_orphan{$file} = "$dir_tran/$dir,$file.$ext_tran";
	}
    }
}

# Iterate again to deal with orphaned files now that they've been collected
foreach $dir (sort (keys %assoc_main_orig))
{
    foreach $file (sort (@{$assoc_main_orig{$dir}}))
    {
	if ( $assoc_main_orphan{$file} )
	{
	    if ( $assoc_tran_orphan{$file} )
	    {
		# The new mothership/main orphan is a moved file
		my @tran_parts	= split(/\,/, $assoc_tran_orphan{$file});
		my $tran_path	= "$tran_parts[0]$tran_parts[1]";
		print "  <-> New Orphan was moved - $dir_tran/$dir$file \n";

		# Merge the existacing translated file with the moved one
  		do_the_merge(1,
			     "$dir$file",
			     "$tran_path",
			     "$assoc_main_orphan{$file}",
			     "$dir_tran/$dir$file");
		# Remove 'em from our list, they been dealt with
		delete($assoc_main_orphan{$file});
		delete($assoc_tran_orphan{$file});

		# Take care of the now dealt with translated file
		&dispose_of_older_files($tran_parts[0], $tran_parts[1]);
	    }
	    else
	    {
		# The new mothership/main orphan is really a new file
		my $filename = "$dir_tran/$dir$file.$ext_orig";
		print "  <-> New Orphan copied - $filename \n";
		if ( !(copy ("$dir_main/$dir$file.$ext_orig",
			     "$dir_tran/$dir$file.$ext_orig")) )
		{
		    &err("Can't copy $file : $!\n");
		}
	    }
	}
    }
}

# Deal with the rest of the unclaimed translated files		
foreach $file (sort (keys %assoc_tran_orphan))
{
    my @tran_parts = split(/\,/, $assoc_tran_orphan{$file});
    &dispose_of_older_files($tran_parts[0], $tran_parts[1]);
}

##
# If there are outdated directories - tag 'em with REM_
if ( $old_rename )
{
    foreach $dir (sort (keys %assoc_local_tran))
    {
	if (! $assoc_main_orig{$dir} )
	{
	    my $src_obj = "$dir_tran/$dir";
	    print "  <-> Outdated tran dir (renamed w/ REM_) - $src_obj \n";
	    rename("$dir_tran/$dir",
		   "$dir_tran/REM_$dir");

	    # Remove from our list any dir already dealt with (for next step)
	    delete($assoc_local_orig{$dir});
	}
    }
    foreach $dir (sort (keys %assoc_local_orig))
    {
	if (! $assoc_main_orig{$dir} )
	{
	    my $src_obj = "$dir_tran/$dir";
	    print "  <-> Outdated orig dir (renamed w/ REM_) - $src_obj \n";
	    rename("$dir_tran/$dir",
		   "$dir_tran/REM_$dir");
	}
    }
}

# Report out run time to user
&report_run_time($start_time);
exit(0);

       ###        ###
######## Procedures ########
       ###        ###

##
# Process original (non-translated) file
sub proc_downloaded
{
    my (
        $filename,
        $sub_out,
        $sub_dir,
       );

    if ( ($File::Find::dir  !~ /CVS/) &&
	 ($File::Find::name =~ /\.$ext_orig($)/) )
    {
        $filename	= (split (/\./, $_))[0];

        $sub_out	= (split(/$dir_main\//, $File::Find::dir))[1];
        $sub_dir	= ( $sub_out ? "$sub_out/" : "" );

        push(@{$assoc_main_orig{$sub_dir}}, $filename);

        # Keep track of files found
        $assoc_main_orig_found{"$sub_dir$filename"} = 1;
#        print "Found .$ext_orig in ($sub_dir, $filename)\n";
    }
}

##
# Process un-translated local file
sub proc_local_orig
{
    my (
        $filename,
        $sub_out,
        $sub_dir,
       );

    if ( ($File::Find::dir  !~ /CVS/) &&
	 ($File::Find::name =~ /\.$ext_orig($)/) )
    {
        $filename	= (split (/\./, $_))[0];

        $sub_out	= (split(/$dir_tran\//, $File::Find::dir))[1];
        $sub_dir	= ( $sub_out ? "$sub_out/" : "" );

        push(@{$assoc_local_orig{$sub_dir}}, $filename);

        # Keep track of files found
        $assoc_local_orig_found{"$sub_dir$filename"} = 1;
#        print "Found .$ext_orig in ($sub_dir, $filename)\n";
    }
}

##
# Process translated local file
sub proc_local_tran
{
    my (
        $filename,
        $sub_out,
        $sub_dir,
       );

    if (  ($File::Find::dir  !~ /CVS/) &&
	  ($File::Find::name =~ /\.$ext_tran($)/) )
    {
        $filename	= (split (/\./, $_))[0];

        $sub_out	= (split(/$dir_tran\//, $File::Find::dir))[1];
        $sub_dir	= ( $sub_out ? "$sub_out/" : "" );

        push(@{$assoc_local_tran{$sub_dir}}, $filename);

        # Keep track of files found
        $assoc_local_tran_found{"$sub_dir$filename"} = 1;
#        print "Found .$ext_tran in ($sub_dir, $filename)\n";
    }
}

##
# Run the message merge utility
sub do_the_merge
{
    my ($forced_move,
	$file_name,
	$dir_tran,
	$dir_main,
	$dir_to) = @_;

    # Come on, run the bloody msgmerge
    print "  <+> MsgMerge invoked,\n";
    print "      - $dir_tran\n";
    print "      - $dir_main\n";

    # Deal with the 'msgmerge' output
    open (LOG, ">> $merge_log") or die "Can't open $merge_log: $!\n";
    print LOG "--- Processing,\n";
    print LOG "  - $dir_tran\n";
    print LOG "  - $dir_main\n";
    close (LOG);
    $log_string = ">> $merge_log 2>&1";

    system("msgmerge \\
            $dir_tran \\
            $dir_main \\
            -o $dir_to.merge $log_string");

    # Update only when file needs to be updated
    my $diff = `diff $dir_tran \\
                     $dir_to.merge`;
    if ( $diff || $forced_move )
    {
	print "  <.> MsgMerge results kept - $file_name.$ext_tran \n";
	rename("$dir_to.merge",
	       "$dir_to.$ext_tran");
    }

    unlink("$dir_to.merge");
}

##
# Find out what to do with files that are of no concern to us now
sub dispose_of_older_files
{
    my ($old_dir,
	$old_filename) = @_;

    if ( $old_rename )
    {
	print "  <-> Outdated (renamed w/ REM_) - ${old_dir}$old_filename \n";
	rename("${old_dir}$old_filename",
	       "${old_dir}REM_$old_filename");
    }
    else
    {
	# Delete actual file - don't know if this is really a good idea !!
	print "  <-> Outdated (deleting file) - ${old_dir}$old_filename \n";
#	unlink("$old_dir/$old_filename");
    }
}

##
# Be consistent in error message reporting
sub err
{
    my ($err_msg)	= @_;

    die "<<!>> ERROR($this_script): $err_msg";
}

##
# Report the total run-time of the script
sub report_run_time
{
    my ($begin_time) = @_;
    my ($end_time,
	$total_time);
 
    $end_time = time;
    $total_time = $end_time - $begin_time;
    if (($total_time/3600) > 1)     # hours
    {
        printf "<< * >> Total run-time = %.2f",($total_time/3600);
        print " hours\n";
    }
    elsif (($total_time/60) > 1)    # minutes
    {
        printf "<< * >> Total run-time = %.2f",($total_time/60);
        print " minutes\n";
    }
    else                            # seconds
    {
        print "<< * >> Total run-time = $total_time seconds\n";
    }
}

##
# Display this script's usage (with or without a death afterwards)
sub usage
{
    my ($die_after) = @_;

    print "Usage: $this_script <-main_dir download_path>\n";
    print "       $this_spaces <-tran_dir translate_path>\n";
    print "       $this_spaces [-ext_main extension_string]\n";
    print "       $this_spaces [-log <merge_filename>]\n";
    print "       $this_spaces [-flag_old_files]\n";

    if ( $die_after )
    {
	exit(5);
    }
}

##
# Print one-liner help
sub help
{
    &usage(0);

    print qq|
-> update and sync-up files downloaded with main repository (mother-ship)

  Options:

    <-main_dir path>	: Main (mother-ship) file directory
    <-tran_dir path>	: Translated         file directory
    [-ext_main string]	: Main file extension         (default uses "pot")
    [-log <filename>]   : Capture msgmerge output log (default off)
    [-keep_old_files]	: Don't rename outdated files (default uses "REM_")

|;
    exit(10);
}

##
# Get the command line arguments
sub get_args
{
  &NGetOpt (
	    "help",			# Print a help screen
	    "main_dir=s",		# Main (mother-ship) file directory
            "tran_dir=s",		# Translated         file directory
            "ext_main=s",		# Main file extension to use
	    "log:s",                    # msgmerge log enabler & optional name
            "keep_old_files",		# Don't rename outdated files
           ) || ( $? = 257, die "Invalid argument\n" );
}
