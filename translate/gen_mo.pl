#!/usr/bin/perl
# -*-Perl-*-

#---
# $Id$
#
# ------------
# Description:
# ------------
#  Run a specific Unix command on a predefined set of files with a
#  common extension.
#
# -----------------
# Revision Details:    (Updated by Revision Control System)
# -----------------
#  $Date$
#  $Author$
#  $Revision$
#  $Source$
#
# (www.arabeyes.org - under GPL license - (c) Nadim Shaikli)
#---

require "ctime.pl";
require "newgetopt.pl";

use English;
use File::Find;

##
# Keep track of run-time
$glb_start_time	= time;

##
# Specify global variable values and init some Variables
$this_script    = $0;
$this_script    =~ s|^.*/([^/]*)|$1|;

##
# Find how many spaces the scripts name takes up (nicety)
$this_spaces    = $this_script;
$this_spaces    =~ s/\S/ /g;

# Get some background info
chop($date = `date -u`);
chop($pwd  = `pwd`);

# Process the command line
&get_args();

if (!$opt_dir) { &usage(1); }
if ($opt_help) { &help; }

print "<< * >> Using $this_script$version\n";

# Variables defined along with some User specified options
$cmd		= $opt_cmd	|| "msgfmt";
$tran_ext 	= $opt_ext	|| "po";
$prepend_dir	= $opt_prepend	|| "";

if (!-d $opt_dir )
{
    die " <<!>> ERROR($this_script): dir '$opt_dir' not found \n";
}

# Generate/Process directory with all the files (generate arrays)
find(\&proc_orig, $opt_dir);

# Make sure we have something to run with
if (! defined %assoc_array_dirs)
{
    die " <<!>> ERROR($this_script): NO '$tran_ext' files in $opt_dir\n";
}

# Execute the unix commands
&run_command();

if ($opt_prepend)
{
    print "  <*> All files written into '$opt_prepend' hierarchy. \n";
}
# Tell 'em how long the whole we were ON
&report_run_time($glb_start_time);

exit(0);

       ###        ###
######## Procedures ########
       ###        ###

##
# Process sought after files (build-up the arrays)
sub proc_orig
{
    my (
	$my_file,
	$my_dir,
       );

    # Grab all files that end with my extension
    if ( ($File::Find::name =~ /\.$tran_ext$/) )
    {
	$my_file	= $_;
	$my_dir		= $File::Find::dir;

	# Build the acutal array - per dir
	push(@{$assoc_array_dirs{$my_dir}}, $my_file);
    }
}

##
# Run the acutal unix commands
sub run_command
{
    my (
	$dir,
	$file,
       );

    # Deal with the optional pre-append option
    if ($opt_prepend)
    {
	# See if the dir doesn't exist
	if (!-d $opt_prepend)
	{
	    # Is User conscious of its non-existance
	    if ($opt_force)
	    {
		mkdir("$opt_prepend", 0755);
	    }
	    else
	    {
		die " <<!>> ERROR($this_script): dir '$opt_prepend' not found \n";
	    }
	}

	# If user didn't enter an ending slash, add it for him
	if ($opt_prepend !~ /\/$/)
	{
	    $opt_prepend = "$opt_prepend/";
	}
    }

    # Cycle through all the captured directories
    foreach $dir (sort (keys %assoc_array_dirs))
    {
	# Check for appended directories existance
	if ($opt_prepend && (!-d "${opt_prepend}$dir"))
	{
	    print "  <+> Creating dir '${opt_prepend}$dir' \n";

	    # Account for missing parents
	    system "mkdir -p ${opt_prepend}$dir";
	}

	foreach $file (sort (@{$assoc_array_dirs{$dir}}))
	{
	    $bare_file	= (split(/\./, $file))[0];

	    print "  < > Processing $dir/$file \n";
	    system "$cmd $dir/$file -o ${opt_prepend}$dir/$bare_file.mo";
	}
    }
}

##
# Report the total run-time of the script
sub report_run_time
{
    my ($begin_time)    = @_;

    my (
        $end_time,
        $total_time
       );

    $end_time	= time;
    $total_time = $end_time - $begin_time;

  TIME_CASE:
    {
        # Hours
        if (($total_time/3600) > 1)
        {
            printf "<< * >> Run-time = %.2f hours\n",($total_time/3600);
            last TIME_CASE;
        }
        # Minutes
        if (($total_time/60) > 1)
        {
            printf "<< * >> Run-time = %.2f minutes\n",($total_time/60);
            last TIME_CASE;
        }
        # Seconds
        print "<< * >> Run-time = $total_time seconds\n";
    }
}

##
# Print short usage info
sub usage
{
    my ($die_after) = @_;

    print qq
|Usage: $this_script <-dir dirname>  [-cmd unix_command]
       $this_spaces                 [-ext file_ext]
       $this_spaces                 [-prepend dirname]
       $this_spaces                 [-force]
       $this_spaces                 [-debug]
       $this_spaces                 [-help]
|;

    if ( $die_after ) { exit(5); }
}

##
# Print one-liner help
sub help
{
    &usage(0);

    print qq|
-> generates a bar-graph for a specified translation project

  Options:

    <-dir dirname>          : Specify directory of where files reside
    [-cmd unix_command]     : Specify the Unix command to run
    [-ext file_ext]         : Specify file extension of translated file (po)
    [-prepend dirname]      : Specify to pre-append output to a new directory
    [-force]                : Specify to create any new dirs if not found
    [-debug]                : Specify debug mode (no output file or db update)
    [-help]                 : Specify to print this help menu

|;
    exit(10);
}

##
# Get the command line arguments
sub get_args
{
  &NGetOpt (
	    "dir=s",			# project directory (where files are)
	    "cmd=s",			# unix command to run
	    "ext=s",			# original file extension
	    "prepend=s",		# indicate what dir to prepend to
	    "force",			# indicate what dir to prepend to
	    "debug",			# debug mode - don't update file/db
	    "help",                     # print a nice help screen
	   ) || ( $? = 257, die "Invalid argument\n" );
}
