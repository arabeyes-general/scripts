#!/usr/bin/perl
# -*-Perl-*-

#---
# $Id$
#
# ------------
# Description:
# ------------
#  A script to check that msgstr's are indeed Arabic strings.
#
#  A -replace will rename the original file (append a .orig)
#  and replaces the file's content with the new text.
#
# -----------------
# Revision Details:    (Updated by Revision Control System)
# -----------------
#  $Date$
#  $Author$
#  $Revision$
#  $Source$
#
# (www.arabeyes.org - under GPL license - (c) Nadim Shaikli)
#---

require "newgetopt.pl";

use English;
use File::Find;

##
# Keep track of run-time
$start_time     = time;

##
# Specify global variable values and init some Variables
$this_script	= $0;
$this_script	=~ s|^.*/([^/]*)|$1|;
$version	= '$Revision$';
$version	=~ s/.*:\s+(\S+)\s+.*/ (version - $1)/;

##
# Find how many spaces the scripts name takes up (nicety)
$this_spaces    = $this_script;
$this_spaces    =~ s/\S/ /g;

chop($date = `date`);
chop($pwd  = `pwd`);

# Process the command line
&get_args();

if ( $opt_help ) { &help; }

# File extensions being dealt with
$ext_orig	= "pot";
$ext_tran	= "po";

# Remove the last slash if there was one (breaks things)
$dir_main       = $opt_dir || "./";
$dir_main	=~ s|/$||;

# If this is a relative path, absolute it !!
if ( $dir_main !~ /^\// )
{
    $dir_main	= "$pwd/$dir_main";
}

if (!-d $dir_main )
{
   die "<<!>> ERROR($this_script): $dir_main doesn't exist \n";
}

# Let user know that script is running
print "<< * >> Using $this_script$version\n";

# Generate/Process directory with all the files (generate arrays)
find(\&proc_dir, $dir_main);

if (! defined %assoc_files)
{
    die "<<!>> ERROR($this_script): NO files were found in $dir_main\n";
}

# Here's the main heart of what is done
&proc_files(\%assoc_files);

# Report out run time to user
&report_run_time($start_time);

exit(0);

       ###        ###
######## Procedures ########
       ###        ###

##
# Process the directory to attain all relevant files
sub proc_dir
{
    my (
        $filename,
        $sub_out,
        $sub_dir,
       );

    if ( ($File::Find::dir  !~ /CVS/) &&
	 (($File::Find::name =~ /\.$ext_orig($)/ ||
	  ($File::Find::name =~ /\.$ext_tran($)/))) )
    {
        $filename	= (split (/\./, $_))[0];

        $sub_out	= (split(/$dir_main\//, $File::Find::dir))[1];
        $sub_dir	= ( $sub_out ? "$sub_out/" : "" );

        push(@{$assoc_files{"$File::Find::dir/"}}, $_);

        # Keep track of files found
        $assoc_found{"$sub_dir$filename"} = 1;
    }
}

##
# Process all the files in each of the directories
sub proc_files
{
    my ($ref_assoc_files)	= @_;

    my $assoc_files = %{$ref_assoc_files};

	foreach my $dir (sort (keys %assoc_files))
	{
	    print "Processing dir - $dir \n";
	    foreach my $file (sort (@{$assoc_files{$dir}}))
	    {
		($num_modified,
		 $ref_outarray)	= &proc_infile($dir, $file);

		&proc_outfile($num_modified,
			      $ref_outarray,
			      "${dir}$file");
	    }
	}
}

##
# Process all input files and their strings 
sub proc_infile
{
    my ($dir, $file) = @_;

    my (
	@char,
	@out,
	$do,
	$punc_num,
	$punc_ok,
	$i,
	$cur,
	$modified,
       );

    print "  Processing file - $file ";

    open (INFILE, "< ${dir}$file") or die "Can't open ${dir}$file \n";

    while (<INFILE>)
    {
	if (/^msgstr\s+\"(.*)\"/)
	{
	    @char = split ("", $1);

	    if (!@char)
	    {
		push (@out, $_);
		next;
	    }

	    $do		= 1;
	    $punc_num	= 0;
	    $punc_ok	= 0;
	    for ($i = 0; $i <= $#char && $do; $i++)
	    {
		# Get the hex values of all the characters
		$cur = unpack("U", $char[$i]);
		$cur = sprintf "\U%x\E", $cur;
		$cur = hex($cur);

		if ($cur == 0xD9 || $cur == 0xD8)
		{
		    # Found something Arabic
		    $do = 0;
		    push (@out, $_);
		    next;
		}

		if ( ($cur > 0x20 && $cur < 0x2F) ||
		     ($cur > 0x3A && $cur < 0x40) ||
		     ($cur > 0x5B && $cur < 0x60) ||
		     ($cur > 0x7B && $cur < 0x7E) )
		{
		    # Found some punctuation
		    $punc_num++;

		    if ($punc_num == ($#char + 1))
		    {
			# Term is entirely punctuation, its OK then
			$punc_ok	= 1;
			push (@out, $_);
			next;
		    }
		}

		if (($i == $#char) && !$punc_ok )
		{
		    # Found no Arabic, empty it out
		    push (@out, "msgstr \"\"\n");
		    $modified++;
		    next;
		}
	    }

	    # Get next line
	    next;
	}

	# Grab the line and save it off
	push (@out, $_);
    }
    close (INFILE);

    return($modified, \@out);
}

##
# Process all output files (ie. generate any output)
sub proc_outfile
{
    my ($modified,
	$ref_out,
        $full_filename)	= @_;

    my @out	= @{$ref_out};

    my (
	$outfile,
	$line,
       );

    if ($modified)
    {
	if ($opt_replace)
	{
	    $outfile	= $full_filename;
	    rename($full_filename, "$full_filename.orig");
	}
	else
	{
	    $outfile	= "$full_filename.new";
	}

	open (OUT, "> $outfile") or die "Can't open $outfile \n";

	foreach $line (@out)
	{
	    print OUT $line;
	}
	close (OUT);
	print "(modified)\n";
    }
    else
    {
	print "(clean :-)\n";
    }
}

##
# Report the total run-time of the script
sub report_run_time
{
    my($begin_time) = @_;
    my($end_time);
    my($total_time);
 
    $end_time = time;
    $total_time = $end_time - $begin_time;
    if (($total_time/3600) > 1)     # hours
    {
        printf "<< * >> Total run-time = %.2f",($total_time/3600);
        print " hours\n";
    }
    elsif (($total_time/60) > 1)    # minutes
    {
        printf "<< * >> Total run-time = %.2f",($total_time/60);
        print " minutes\n";
    }
    else                            # seconds
    {
        print "<< * >> Total run-time = $total_time seconds\n";
    }
}

##
# Display this script's usage (with or without a death afterwards)
sub usage
{
    my ($die_after) = @_;

    print "Usage: $this_script <-dir directory>\n";
    print "       $this_spaces <-replace>\n";

    if ( $die_after )
    {
	exit(5);
    }
}

##
# Print one-liner help
sub help
{
    &usage(0);

    print qq|
-> validate that all .pot and .po files have Arabic msgstr text

  Options:

    <-dir directory>	: Specify directory to process (default - ".")
    <-replace>		: Rename original files and replace their content

|;
    exit(10);
}

##
# Get the command line arguments
sub get_args
{
  &NGetOpt (
	    "help",			# Print a help screen
	    "dir=s",			# Directory name to process
	    "replace",			# Indicate to replace original file
	    "keep",			# Indicate to retain original file
           ) || ( $? = 257, die "Invalid argument\n" );
}
