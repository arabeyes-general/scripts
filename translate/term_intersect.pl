#!/usr/bin/perl
# -*-Perl-*-

#---
# $Id$
#
# ------------
# Description:
# ------------
#  This script will read two directory trees and extract exact string
#  matches and translations (if any) to populate a 'list' output.
#  These strings are noted within as 'terms'.
#
# -----------------
# Revision Details:	(Updated by Revision Control System)
# -----------------
#  $Date$
#  $Author$
#  $Revision$
#  $Source$
#
#  (www.arabeyes.org - under GPL license - (c) Nadim Shaikli)
#---

require "ctime.pl";
require "newgetopt.pl";

use English;
use File::Find;

##
# Keep track of run-time
$glb_start_time	= time;

##
# Specify global variable values and init some Variables
$in_script	= $0;
$in_script	=~ s|^.*/([^/]*)|$1|;

##
# Find how many spaces the scripts name takes up (nicety)
$in_spaces	= $in_script;
$in_spaces	=~ s/\S/ /g;

##
# Save off command line options to echo 'em back in generated file
$cmdline	= "# command-line options used:\n";
foreach $input (@ARGV)
{
    if ($input =~ /^-/)
    {
	if ($prev_dash)
	{
	    $cmdline .= "\n";
	}
	$prev_dash	= 1;
	$cmdline        .= "#     ";
    }

    $cmdline	.= "$input ";
    if ($input !~ /^-/)
    {
	$prev_dash	= 0;
	$cmdline	.= "\n";
    }
}

##
# Process the command line
&get_args();

if ($opt_quiet)
{
    # For quiet mode
    use FileHandle;
    STDOUT->autoflush(1);
}

if ( $opt_help ) { &help; }

if (!$opt_dir1 || !$opt_dir2) { &usage(1); }

print "<< * >> Using $in_script\n";

# Variables defined along with some User specified options
$tran_ext 	= $opt_ext		|| "po";
$max_term_len	= $opt_max_term		|| "3";
$do_same_proj	= $opt_same_proj	|| "0";
$out_file	= $opt_output		|| "common_terms";
$out_ext	= ".po";

# Generate/Process directory with all the files (generate hashes)
find(\&proc_dir1, $opt_dir1);
find(\&proc_dir2, $opt_dir2);

# Make sure we have something to work with
if (! defined %assoc_array_dir1)
{
    die " <<!>> ERROR($in_script): NO '$tran_ext' files in $opt_dir1\n";
}
if (! defined %assoc_array_dir2)
{
    print " <<!>> WARNING($in_script): NO '$tran_ext' files in $opt_dir2\n";
    print " <<?>> Would you like to continue anyways [Y/n]: ";

    # See if the user wants to continue (he'd be only using dir1's contents)
    $move_on	= <STDIN>;

    if ($move_on =~ /n/i)
    {
	die "<< * >> User opted exit.\n";
    }
}

# Make sure user knows what we are after
print " < * >  Searching for terms that are $max_term_len words or less\n";

# Process the dirs (and files) to generate a list of the terms
$ref_terms_dir1	= &proc_dir(\%assoc_array_dir1);
$ref_terms_dir2	= &proc_dir(\%assoc_array_dir2);

# De-reference hashes
%terms_dir1	= %{$ref_terms_dir1};
%terms_dir2	= %{$ref_terms_dir2};

# Process the arrays and dump out the outputs
$final_outfile = &intersect_em(\%terms_dir1, \%terms_dir2);

print " < + >  Total num of skipped terms = $total_skipped\n";
print " < * >  Generated file '$final_outfile' of matched terms\n";

# Tell 'em how long the whole we were ON
&report_run_time($glb_start_time);

exit(0);

       ###        ###
######## Procedures ########
       ###        ###

##
# Process sought-after files in dir1 (build-up the arrays)
sub proc_dir1
{
    my (
	$my_file,
	$my_dir,
       );

    # Grab all files that end with my extension
    if ( ($File::Find::name =~ /\.$tran_ext$/) )
    {
	$my_file = $_;
	$my_dir  = $File::Find::dir;

	# Build the acutal array - per dir
	push(@{$assoc_array_dir1{$my_dir}}, $my_file);
    }
}

##
# Process sought-after files in dir2 (build-up the arrays)
sub proc_dir2
{
    my (
	$my_file,
	$my_dir,
       );

    # Grab all files that end with my extension
    if ( ($File::Find::name =~ /\.$tran_ext$/) )
    {
	$my_file = $_;
	$my_dir  = $File::Find::dir;

	# Build the acutal array - per dir
	push(@{$assoc_array_dir2{$my_dir}}, $my_file);
    }
}

##
# Process the passed-in directory for files
sub proc_dir
{
    my ($ref_assoc_array_dirs) = @_;

    # Define some local variables
    my (
	%assoc_array_dirs,
	$dir,
	$file,
	$ref_terms_read,
	%terms_read,
	$mode,
	$cnt,
       );

    %assoc_array_dirs = %{$ref_assoc_array_dirs};

    # Cycle through all the captured directories
    foreach $dir (sort (keys %assoc_array_dirs))
    {
	# Check that each dir is a true dir (sanity check)
	if (!-d $dir) { die " <<!>> ERROR($in_script) Can't open dir $dir\n"; }

	foreach $file (sort (@{$assoc_array_dirs{$dir}}))
	{
	    if ($opt_quiet)
	    {
		$mode = &active_dance($mode, $cnt++ % 7, "\.");
	    }
	    $ref_terms_read = &proc_files("$dir/$file", \%terms_read);
	    %terms_read	    = %{$ref_terms_read};
	}

	if ($opt_quiet)
	{
	    print "\r        \r";
	    $mode = 0;
	    $cnt = 0;
	}
    }
    return(\%terms_read);
}

##
# Process the passed-in file for terms
sub proc_files
{
    my ($in_filename,
	$ref_assoc_new)	= @_;

    my (
	%assoc_new,
	$line_num,
	@words,
	$term_msgid,
	$local_skipped,
	$local_accepted,
	$tran,
	$have_it_already,
	$entry,
       );

    %assoc_new = %{$ref_assoc_new};

    if (!$opt_quiet) { print " < - >  Processing - $in_filename \n"; }

    # Grab file with certain extensions
    if ($in_filename =~ /\.po/)
    {
	open (INFILE, "< $in_filename") or
	    die " <<!>> ERROR($in_script): Can't open $in_filename - $!";

	# Grok the passed-in file
	while (<INFILE>)
	{
	    $line_num++;
	    chomp;

	    # Try to sped-up the reading of the file
	    if (/^\#/ || /^\s+/)
	    {
		next;
	    }

	    # - Grab the term/string
	    if (/^msgid\s+\"(\S+.*)\"/)
	    {
		# See that we are within certain upper term count
		@words = split(/\s+/, $1);
		if ($#words > $max_term_len-1)
		{
		    undef($term_msgid);
		    $local_skipped++;
		    next;
		}
		$local_accepted++;
		$term_msgid	= uc($1);
		$assoc_new{$term_msgid}{'msgid'} = $1;

 		# In case the same term is in multi-files, array it
		push(@{$assoc_new{$term_msgid}{'pwd'}},
		     "$in_filename:+:$line_num");
	    }
	    if (/^msgstr\s+\"(.*)\"/ && $term_msgid)
	    {
		$tran = $1;

		# Make sure I have something of value
		if ($tran =~ /\S+/)
		{
 		    $have_it_already = 0;
 		    foreach $entry (@{$assoc_new{$term_msgid}{'msgstr'}})
 		    {
 			if ($entry eq $tran)
 			{
			    $have_it_already = 1;
 			}
 		    }
		    if (!$have_it_already)
		    {
			# In case the same term is in multi-files, array it
			push(@{$assoc_new{$term_msgid}{'msgstr'}}, $tran);
		    }
		}
		# Done with the pair
		undef($term_msgid);
	    }
	}
	close(INFILE);
    }

    if (!$opt_quiet)
    {
	if ($local_skipped)
	{
	    print "       < . > $local_skipped skipped terms (too long)\n";
	}
	print "       < + > $local_accepted matched terms \n";
    }

    $total_skipped += $local_skipped;
    return (\%assoc_new);
}

##
# Print progress characters to show user something is happening
sub active_dance
{
    my ($mode,
	$num,
	$char) = @_;

    my $new_mode = ( !$num ? !$mode : $mode );

    print ($new_mode ? "$char" : "\b \b");

    return($new_mode);
}

##
# Print out the .po header
sub gen_output_head
{
    my ($ref_filehandler,
	$out_filename) = @_;

    print $ref_filehandler qq|# Translation of $out_filename to Arabic
#-*-
# --
# \$Id\$
# --
#
# This is a script generated file (by '$in_script')
#
$cmdline
#
# the file contains a list of terms commonly used in various
# projects.  The idea here is to force consistancy across
# a project and across all translation works conducted by
# Arabeyes.org.
#
# - Copyright (C) 2004 Free Software Foundation, Inc.
#
# To generate a simple equality list, run this command-line
#   perl -n -e 'if (/^msgid\s+"(.*)"/)  { print "$1 = "; } \
#               if (/^msgstr\s+"(.*)"/) { print "$1\\n"; }' \
#               this_file_name_here
#
#-*-
msgid ""
msgstr ""
"Project-Id-Version: $out_filename\\n"
"POT-Creation-Date: 2004-01-02 18:00-0700\\n"
"PO-Revision-Date : 2004-01-03 11:15-0700\\n"
"Last-Translator: You Here <you\@email.org>\\n"
"Language-Team: Arabic <doc\@arabeyes.org>\\n"
"MIME-Version: 1.0\\n"
"Content-Type: text/plain; charset=UTF-8\\n"
"Content-Transfer-Encoding: 8bit\\n"
"X-Generator: KBabel 1.0.1\\n"

|;
}

##
# Find and dump out terms that match
sub intersect_em
{
    my ($ref_assoc_dir1,
	$ref_assoc_dir2) = @_;

    my (
	%assoc_dir1,
	%assoc_dir2,
	%assoc_gotit,
	$first_run,
	$num_terms,
	$num_ext,
	$num_ext_used,
	$outfile,
	$have_a_tran,
	$local_match,
       );

    %assoc_dir1		= %{$ref_assoc_dir1};
    %assoc_dir2		= %{$ref_assoc_dir2};

    # Indicate first cycle through our loop below
    $first_run		= 1;
    $local_match	= 0;

    # Run through the dir1 and dir2 contents
    # - dir1 might have terms within itself that don't intersect with dir2
    # - output printing should proceed with dir1 then dir2 (for sanity's sake)
    foreach my $term (sort (keys %assoc_dir1))
    {
	# Specify output filename based on run-mode and number of terms
	if ( $first_run ||
	     ((defined $opt_num_out) && $num_terms >= $opt_num_out) )
	{
	    # On consecutive or multi runs/files due to number of terms
            # dumps out some info
	    if ( !$first_run )
	    {
		print " < + >  Total num of matched terms = $local_match \n";
		print " < + >  Total num of skipped terms = $total_skipped\n";
		print " < * >  Generated file '$outfile' of matched terms\n";
		close (OUTF);
	    }

	    # Attach a number to output filenames if user uses '-num'
	    if ( defined $opt_num_out )
	    {
		$num_ext	= ( (defined $num_ext) ? $num_ext + 1 : 1);
		$num_ext_used	= "_$num_ext";
	    }

	    # Generate a new file name based on user's command-line
	    $outfile	= "$out_file$num_ext_used$out_ext";

	    # Open an output file for printing
	    open (OUTF, "> $outfile") or
		die "<<!>> ERROR($in_script): Can't open $outfile - $!";

	    # Regurgitate the output
	    &gen_output_head(\*OUTF, $outfile);

	    $first_run	= 0;
	    $num_terms	= 0;
	}

	# Indicate if I have a translation already
	$have_a_tran = 0;

	# See if a term exists in both dir trees or if user wants, single dir
 	if ( (defined $assoc_dir2{$term}{'msgid'}) ||
 	     ($do_same_proj && ($#{$assoc_dir1{$term}{'pwd'}} > 1)) )
	{
	    $local_match++;
	    $num_terms++;

	    # Tell 'em where the string came from
	    # - for dir1
	    foreach my $entry (@{$assoc_dir1{$term}{'pwd'}})
	    {
		my ( $pwd, $line_num ) = split(/\:\+\:/, $entry);
		print OUTF "# From - $pwd ($line_num)\n";
	    }
	    # - for dir2
	    foreach my $entry (@{$assoc_dir2{$term}{'pwd'}})
	    {
		my ( $pwd, $line_num ) = split(/\:\+\:/, $entry);
		print OUTF "# From - $pwd ($line_num)\n";
	    }

	    # Deal with the terms and translations
	    print OUTF "msgid \"$assoc_dir1{$term}{'msgid'}\" \n";

	    # Keep track of whether 'msgstr ' was printed or not
	    my $string_printed	= 0;

	    # - for dir1
	    foreach my $msgstr (@{$assoc_dir1{$term}{'msgstr'}})
	    {
		$have_a_tran = 1;
		if (!$string_printed)
		{
		  print OUTF "msgstr ";
		  $string_printed = 1;
		}
		print OUTF "\"$msgstr\"\n";

		# Don't print same translation output
		$assoc_gotit{$msgstr} = 1;
	    }
	    # - for dir2
	    foreach my $msgstr (@{$assoc_dir2{$term}{'msgstr'}})
	    {
		if (!$assoc_gotit{$msgstr})
		{
		    $have_a_tran = 1;
		    if (!$string_printed)
		    {
		      print OUTF "msgstr ";
		      $string_printed = 1;
		    }
		    print OUTF "\"$msgstr\"\n";
		}
	    }

	    # Make sure there is a 'msgstr'
	    if (!$have_a_tran)
	    {
		print OUTF "msgstr \"\" \n";
	    }
	    print OUTF "\n";
	}
    }

    close(OUTF);

    print " < + >  Total num of matched terms = $local_match \n";
    return($outfile);
}

##
# Report the total run-time of the script
sub report_run_time
{
    my ($begin_time)    = @_;

    my (
        $end_time,
        $total_time,
       );

    $end_time = time;
    $total_time = $end_time - $begin_time;
  TIME_CASE:
    {
        # Hours
        if (($total_time/3600) > 1)
        {
            printf "<< * >> Run-time = %.2f hours\n",($total_time/3600);
            last TIME_CASE;
        }
        # Minutes
        if (($total_time/60) > 1)
        {
            printf "<< * >> Run-time = %.2f minutes\n",($total_time/60);
            last TIME_CASE;
        }
        # Seconds
        print "<< * >> Run-time = $total_time seconds\n";
    }
}

##
# Print short usage info
sub usage
{
    my ($die_after) = @_;

    print qq
|Usage: $in_script <-dir1 directory> <-dir2 directory>
       $in_spaces                    [-output filename]
       $in_spaces                    [-max_terms num]
       $in_spaces                    [-num_out num]
       $in_spaces                    [-same_proj]
       $in_spaces                    [-ext file_ext]
       $in_spaces                    [-quiet]
       $in_spaces                    [-help]
|;

    if ( $die_after ) { exit(5); }
}

##
# Print one-liner help
sub help
{
    &usage(0);

    print qq|
-> process two directory trees to produce a common wordlist within them

  Options:

    <-dir1 directory>  : Specify first  directory tree to process
    <-dir2 directory>  : Specify second directory tree to process
    [-output filename> : Sepcify output filename to use
    [-max_terms num]   : Specify maximum number of words within a term
    [-num_out num]     : Specify number of terms to output per output file
    [-same_proj]       : Specify is to include same project intersections
    [-ext file_ext]    : Specify file extension of files to process
    [-quiet]           : Specify to be quiet during run (!verbose)
    [-help]            : Produce this help screen

|;
    exit(10);
}

##
# Get the command line arguments
sub get_args
{
  &NGetOpt (
            "help",		# print a nice help screen
            "dir1=s",	        # dir1 option
            "dir2=s",		# dir2 option
	    "output=s",		# output filename to use
            "max_term=s",	# maximum allowed number of terms
            "num_out=s",	# number of terms per output file
            "same_proj",	# produce results from same project too ?
            "ext=s",		# file extension to use
            "quiet",		# be quiet and don't print steps
           ) || ( $? = 257, die"<<!>> ERROR($in_script): Invalid argument\n" );
}
