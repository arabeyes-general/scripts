#!/bin/bash

GIT_URL=https://git.arabeyes.org

git clone $GIT_URL/arabeyes-general/www.git
git clone $GIT_URL/arabeyes-general/scripts.git
git clone $GIT_URL/arabeyes-general/tools.git
git clone $GIT_URL/arabeyes-general/ae_admin.git
git clone $GIT_URL/arabeyes-general/doc.git

mkdir art
cd art
git clone $GIT_URL/arabeyes-art/khotot.git
git clone $GIT_URL/arabeyes-art/ae_logos.git
cd ..

mkdir translate
cd translate
git clone $GIT_URL/arabeyes-i18n/actionapps.git
git clone $GIT_URL/arabeyes-i18n/childsplay.git
git clone $GIT_URL/arabeyes-i18n/debian.git
git clone $GIT_URL/arabeyes-i18n/drupal.git
git clone $GIT_URL/arabeyes-i18n/fedora.git
git clone $GIT_URL/arabeyes-i18n/freedesktop.git
git clone $GIT_URL/arabeyes-i18n/gnome.git
git clone $GIT_URL/arabeyes-i18n/kde.git
git clone $GIT_URL/arabeyes-i18n/lyx.git
git clone $GIT_URL/arabeyes-i18n/mandriva.git
git clone $GIT_URL/arabeyes-i18n/manpages.git
git clone $GIT_URL/arabeyes-i18n/misc.git
git clone $GIT_URL/arabeyes-i18n/mozilla.git
git clone $GIT_URL/arabeyes-i18n/openoffice.git
git clone $GIT_URL/arabeyes-i18n/openoffice.old.git
git clone $GIT_URL/arabeyes-i18n/plone.git
git clone $GIT_URL/arabeyes-i18n/qt.git
git clone $GIT_URL/arabeyes-i18n/technicaldictionary.git
git clone $GIT_URL/arabeyes-i18n/wdmanagers.git
git clone $GIT_URL/arabeyes-i18n/wordlist.git
cd ..

mkdir projects
cd projects
git clone $GIT_URL/arabeyes-dev/TashkeelAddOns.git
git clone $GIT_URL/arabeyes-dev/adawat.git
git clone $GIT_URL/arabeyes-dev/akka.git
git clone $GIT_URL/arabeyes-dev/ar-espeak.git
git clone $GIT_URL/arabeyes-dev/arabbix.git
git clone $GIT_URL/arabeyes-dev/arabicdiacassitant.git
git clone $GIT_URL/arabeyes-dev/arabiclightproof.git
git clone $GIT_URL/arabeyes-dev/arramooz.git
git clone $GIT_URL/arabeyes-dev/ayaspell.git
git clone $GIT_URL/arabeyes-dev/bayani.git
git clone $GIT_URL/arabeyes-dev/bicon.git
git clone $GIT_URL/arabeyes-dev/cdmaftooh.git
git clone $GIT_URL/arabeyes-dev/distros.git
git clone $GIT_URL/arabeyes-dev/duali.git
git clone $GIT_URL/arabeyes-dev/edara.git
git clone $GIT_URL/arabeyes-dev/external.git
git clone $GIT_URL/arabeyes-dev/freebsd-ports.git
git clone $GIT_URL/arabeyes-dev/games.git
git clone $GIT_URL/arabeyes-dev/ghalatwai.git
git clone $GIT_URL/arabeyes-dev/hclalexique.git
git clone $GIT_URL/arabeyes-dev/katoob.git
git clone $GIT_URL/arabeyes-dev/mishkal.git
git clone $GIT_URL/arabeyes-dev/morchedtarbawi.git
git clone $GIT_URL/arabeyes-dev/naftawayh.git
git clone $GIT_URL/arabeyes-dev/qalsadi.git
git clone $GIT_URL/arabeyes-dev/qamoose.git
git clone $GIT_URL/arabeyes-dev/quran.git
git clone $GIT_URL/arabeyes-dev/qutrub.git
git clone $GIT_URL/arabeyes-dev/siragi.git
git clone $GIT_URL/arabeyes-dev/thwab.git
git clone $GIT_URL/arabeyes-github/ITL.git
git clone $GIT_URL/arabeyes-github/ITL-programs.git
git clone $GIT_URL/arabeyes-github/ITL-ports.git
cd ..

echo "Git cloning of Arabeyes repos is done."
